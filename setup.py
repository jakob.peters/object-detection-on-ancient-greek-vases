from setuptools import setup

setup(
    name='vase_detection',
    version='0.1.0',    
    description='A Package implementing fasterrcnn, maskrcnn and yolox for object detection on ancient greek vases',
    url='https://github.com/shuds13/pyexample',
    author='Jakob Peters',
    author_email='petersjakobpeters@gmail.com',
    license='BSD 2-clause',
    packages=['vase_detection'],
    install_requires=['mpi4py>=2.0',
                      'numpy',             
                      'yolox',        
                      'albumentations',
                      'seaborn',
                      ],

    classifiers=[
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',  
        'Operating System :: POSIX :: Linux',        
        'Programming Language :: Python :: 3.8',
    ],
)
