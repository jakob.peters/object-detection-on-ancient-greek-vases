# Object Detection on ancient greek vases

## Getting started

This Project needs yolox installed to function. It has been tested on YOLOX 0.3.0. See details and how to install YOLOX on the [YOLOX GitHub Page](https://github.com/Megvii-BaseDetection/YOLOX)
Furthermore This Project needs more Packages such as Albumentations and Seaborn. All packages used in the Conda Environment This project was run in can be seen in the yolox_envronment.yml.

## Preparation of the filesystem

This Project expects images and annotations in the form of json files as used by the Egraphsen project. An example of these json files is provided in the examples folder. To construct the file structure necessary for constructing the dataset you can run clean_data.py. Provide the path to the Annotations, the desired paths for images and jsons for the datasets, as well as the desired path for the outputs and run the script.

## Use this Project

All operations this Project implements are done using one of the Organizer classes in organizer.py. The Organizer is passed a Config class that contains all tunable parameters used for dataset creation, training and inference. See how to construct an organizer for training in fasterrcnn_train.py,maskrcnn_train.py,yolodark_train.py and yoloresnet_train.py. 

infer_and_save_results.py contains an example of how to load a trained model, construct the same dataset used in that training and calculate performance on the test and validation dataset as well as infer and save the images from the test_dataset. To ensure that the same dataset as in training is constructed and to prevent training data leaking into validation or test data it is important not to change the jsons and images in between training and inference.

infer_list_of_images_demo.py contains an example of how to load a trained model without constructing the trainer or dataset. This is done using the LightOrganizer class from organizer.py. naturally no training or inference of the dataset can be done using this class. It is intended to only be used for Detecting objects on a list of images. For the Models implementing a fusion between image data and textual descriptions a list of descriptions can be passed that has to be of the same length as the list of images.

## Using pretrained Models

To use the pretrained models you have to first download the [trained model parameters and pickled config and result files](https://owncloud.gwdg.de/index.php/s/ezLzhAfaMNPLUj6) containing the config file used during training and inference as well as the losses during training and the results of the performance evaluation. Decide which model you want to load, downlaod the trained model parameters and put the file into the model into the models folder created during preparation of the filesystem. Then download the pickled dictionary of the same model and put it into the results folder created during preparation of the filesystem.

Load the pickled dict using [pickle](https://docs.python.org/3/library/pickle.html) and extract the cfg (see infer_list_of_images_dem.py or infer_and_save_results.py).
It is important to provide the cfg with the parameters OUT_DIR, IMAGE_DIR, JSON_DIR, MODELS_DIR, RESULTS_DIR, PLOT_DIR, PREDICTED_IMAGES_DIR and DATASET_DIR as determined during Perparation of the Filesystem, because these paths will be different on your system. If Datasets using the textual descriptions of the images are to be constructed CSV_DIR needs to be set in the cfg. The path should contain two csv files called general.csv and specific.csv. Examples for these files are provided in the examples/csv folder.

## Important

It is always important to configure the paths in the cfg to fit the filesystem on your computer. When training your own model using the Organizer class setting OUT_DIR, IMAGE_DIR, JSON_DIR and eventually CSV_DIR is sufficient. when using pretrained models all the *_DIR parameters of the cfg, except maybe CSV_DIR, need to be set. you can invoke Organizer.save_results() to save the pickled dict including the changed cfg for later. CSV_DIR is only necessary when using the textual descriptions

<!---
## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.gwdg.de/jakob.peters/object-detection-on-ancient-greek-vases/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
--->