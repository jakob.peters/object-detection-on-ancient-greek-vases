import numpy as np
import cv2

def preprocess_image(image,target_height=512,target_width=512,three_channel_trick=True):
    image=unify_image_type(image)
    #start wit image of original size
    height,width,channels=image.shape
    gray_image=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY) # shape [height,width]
    padded_image=np.zeros((max(height,width),max(height,width)),dtype=np.float32)
    padded_image[:height,:width]=gray_image
    
    #raise Exception(f"gray_image_dtype {gray_image.dtype} - min,max {gray_image.min(),gray_image.max()}\npadded_image_dtype {padded_image.dtype} - min,max {padded_image.min(),padded_image.max()}")
    
    if three_channel_trick:
        resized_image=np.zeros((target_height,target_width,channels),dtype=np.float32)
        padded_image=cv2.resize(padded_image,(target_height*2,target_width*2))
        
        resized_image[:,:,0]=padded_image[::2,::2]
        resized_image[:,:,1]=padded_image[1::2,::2]
        resized_image[:,:,2]=padded_image[::2,1::2]
            
            
    else:
        padded_image=cv2.resize(padded_image,(target_height,target_width))
        #gray = cv2.cvtColor(resized_image, cv2.COLOR_BGR2GRAY)
        resized_image=cv2.merge((padded_image,padded_image,padded_image))
        
    return resized_image,height,width

def unify_image_type(image):
    if (image.shape[2]==1):
        image=cv2.cvtColor(image,cv2.COLOR_GRAY2RGB).astype(np.float32)
    elif(image.shape[2]==4):
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB).astype(np.float32)
    elif(image.shape[2]==3):
        image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB).astype(np.float32)
    return image
