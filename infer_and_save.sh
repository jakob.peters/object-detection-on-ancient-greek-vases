#!/bin/bash
#SBATCH -p gpu
#SBATCH -G v100
#SBATCH -N 1
#SBATCH -t 12:00:00
#SBATCH -C scratch
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jakob.peters@phil.uni-goettingen.de
#SBATCH -o ../outfiles/inference-%J.out

module load cuda
module load anaconda3
source $ANACONDA3_ROOT/etc/profile.d/conda.sh
conda activate yolox
conda info
pwd
#nvcc -V
#echo starting cudatest.py
#python cudatest.py
#echo cudatest finished
echo starting inference and saving of results
python infer_and_save_results.py

