
import sys
from model import MyFasterRCNN, FasterRCNNDescriptionModel , MyMaskRCNN, MaskRCNNDescriptionModel
from yolox_model import Resnet50fpnYOLOX
from yolox.models.yolox import YOLOX
from model import create_model as create_default_model
from custom_utils import Averager, SaveBestModel,LossAccumulator, save_model, save_avg_loss_plot, save_accuracy_plot, intersection_over_union_box
from tqdm.auto import tqdm
from datasets import (
    create_dataset, create_loader
)
from transformers import BertTokenizer
import random
import torch
import matplotlib.pyplot as plt
import time
import numpy as np
import torch.nn as nn

from model_utils import create_model,get_optimizer,get_model_and_params,load_model,load_model_state,DescriptionTokenizer
from model_exceptions import *
plt.style.use('ggplot')



def get_trainer(cfg):
    if cfg.DATASET in ['alldescriptions','persondescriptions']:
        return DescriptionObjectDetectionTrainer(cfg)
    else:
        return ObjectDetectionTrainer(cfg)
    

class ObjectDetectionTrainer():
    def __init__(self,
                 cfg,
                ):
        
        cfg.check_cfg()
        self.cfg=cfg
        # configuration variables
        self.intersection_over_union_threshold=cfg.INTERSECTION_OVER_UNION_THRESHOLD
        self.detection_threshold=cfg.DETECTION_SCORE_THRESHOLD
        self.num_workers=cfg.NUM_WORKERS
        self.batch_size=cfg.BATCH_SIZE
        self.device=cfg.DEVICE    
        self.dataset_type=cfg.DATASET
        print("cuda devices:")
        for i in range(torch.cuda.device_count()):
            print(torch.cuda.get_device_properties(i).name)   
        print(f"device is: {self.device}")
        print("\n")
        
        #RPN variables standerd values are fg=0.7 and bg=0.3
        self.rpn_fg_iou_thresh=cfg.RPN_FG_IOU_THRESH
        self.rpn_bg_iou_thresh=cfg.RPN_BG_IOU_THRESH
        #ROI variable standard values are 0.5 and 0.5
        self.box_fg_iou_thresh=cfg.BOX_FG_IOU_THRESH
        self.box_bg_iou_thresh=cfg.BOX_BG_IOU_THRESH
        self.multi_gpu=cfg.USE_MULTI_GPU
        # create datasets and dataloaders
        if cfg.MODEL=="maskrcnn":
            self.do_masks=True
            self.yolo=False
        elif cfg.MODEL=="yolo":
            self.do_masks=False
            self.yolo=True
        else:
            self.yolo=False
            self.do_masks=False
        self.train_dataset = create_dataset(cfg, dataset_mode='train', load_dataset=cfg.LOAD_DATASET, dataset_name=self.dataset_type+"object")
        self.valid_dataset = create_dataset(cfg, dataset_mode='val', load_dataset=True, dataset_name=self.dataset_type+"object")
        self.test_dataset = create_dataset(cfg, dataset_mode='test', load_dataset=True, dataset_name=self.dataset_type+"object")
        if cfg.SHRINK_DATASET_FRACTION:
            self.train_dataset.shrink_dataset(cfg.SHRINK_DATASET_FRACTION)
            self.valid_dataset.shrink_dataset(cfg.SHRINK_DATASET_FRACTION)
            #self.test_dataset.shrink_dataset(cfg.SHRINK_DATASET_FRACTION)
        self.train_loader = create_loader(self.train_dataset, cfg,shuffle=True)
        self.valid_loader = create_loader(self.valid_dataset, cfg)
        self.test_loader = create_loader(self.test_dataset, cfg)
        print(f'finished creating datasets for dataset type {self.dataset_type}')
        if not self.cfg.NUM_CLASSES:
            self.num_classes=len(self.train_dataset.get_classes())
            self.cfg.NUM_CLASSES=self.num_classes
        else:
            self.num_classes=self.cfg.NUM_CLASSES
        
        print(f"number of classes in dataset is {self.num_classes}")
        # define the optimizer hyperparameters
        self.lr=cfg.LR
        self.lr_decay=cfg.LR_DECAY
        self.momentum=cfg.MOMENTUM
        self.weight_decay=cfg.WEIGHT_DECAY
        self.betas=cfg.BETAS
        self.eps=cfg.EPS
        self.initial_accumulator_value=cfg.INITIAL_ACCUMULATOR_VALUE
        self.amsgrad=cfg.AMSGRAD
        self.optimizer_string=cfg.OPTIMIZER_STRING
        
        # build model or load model, build optimizer or load optimizer
        if cfg.LOAD_MODEL_PATH:
            self.model,self.params,optimizer_state_dict=load_model(cfg)            
            self.optimizer,self.optimizer_descriptor=get_optimizer(cfg,self.params)  ## loading of the optimizer state currently disabled
            #print(optimizer_state_dict)
            #self.optimizer.load_state_dict(optimizer_state_dict)
        else:
            self.model,self.params=get_model_and_params(cfg)
            
            self.optimizer,self.optimizer_descriptor=get_optimizer(cfg,self.params)
        
        # initialize the Averager class
        self.train_loss_accumulator=LossAccumulator()
        self.val_loss_accumulator=LossAccumulator()
        # train and validation loss lists to store loss values of all...
        # ... iterations till ena and plot graphs for all iterations
        self.epoch=0
        self.train_accuracy_list=[]
        self.val_accuracy_list=[]
        # name to save the trained model with
        if cfg.MODEL_NAME:
            self.model_name = cfg.MODEL_NAME
        else:
            self.model_name = self.dataset_type+'_detection_model_'+self.optimizer_descriptor
        #initialize savebestmodel class
        self.save_best_model = SaveBestModel(cfg)
        
        
    def set_batch_size(self,batch_size):
        self.batch_size=batch_size
        self.train_loader = create_loader(self.train_dataset, self.num_workers,batch_size)
        self.valid_loader = create_loader(self.valid_dataset, self.num_workers,batch_size)
        self.test_loader = create_loader(self.test_dataset, self.num_workers,batch_size)
        
 
    def get_loader(self,type="train"):
        if type=="train":
            return self.train_loader
        elif type=="valid":
            return self.valid_loader
        elif type=="test":
            return self.test_loader
        else:
            raise InvalidLoaderTypeException
    
    # creates optimizer for the parameters, hyperparameters for the optimizer are taken from the class variabless
    def __get_optimizer(self,params,optimizer_string):
        valid_optimizer_strings=["SGD"]
        if optimizer_string=="SGD":
            optimizer=torch.optim.SGD(params, lr=self.lr, momentum=self.momentum, weight_decay=self.weight_decay)
            optimizer_descriptor=optimizer_string+f'_lr={self.lr}_momentum={self.momentum}_weightdecay={self.weight_decay}'
            print(f'Set optimizer to {optimizer_descriptor}')
            return optimizer, optimizer_descriptor
        elif optimizer_string=="Adam":
            optimizer=torch.optim.Adam(params, lr=self.lr, betas=self.betas, eps=self.eps, weight_decay=self.weight_decay, amsgrad=self.amsgrad)
            optimizer_descriptor=optimizer_string+f'_lr={self.lr}_betas={self.betas}_eps{self.eps}_weightdecay={self.weight_decay}_amsgrad={self.amsgrad}'
            print(f'Set optimizer to {optimizer_descriptor}')
        elif optimizer_string=="Adagrad":
            optimizer=torch.optim.Adagrad(params, lr=self.lr, lr_decay=self.lr_decay, weight_decay=self.weight_decay, initial_accumulator_value=self.initial_accumulator_value, eps=self.eps)
            optimizer_descriptor=optimizer_string+f'_lr={self.lr}_lrdecay={self.lr_decay}_eps{self.eps}_weightdecay={self.weight_decay}_initialaccumulatorvalue={self.initial_accumulator_value}'       
            print(f'Set optimizer to {optimizer_descriptor}')     
        else:
            raise InvalidOptimizerStringException(f"Invalid optimizer string: {optimizer_string}, valid optimizer strings are {valid_optimizer_strings}")
        return optimizer,optimizer_descriptor
        
    def show_optimizer(self):
        print(self.optimizer_descriptor)
        
    def set_optimizer(self,optimizer_string):
        self.optimizer,self.optimizer_descriptor=get_optimizer(self.cfg,self.params)
    def set_lr(self,lr):
        self.lr=lr
        self.set_optimizer(self.optimizer_string)
    def set_momentum(self,momentum):
        self.momentum=momentum
        self.set_optimizer(self.optimizer_string)
    def set_weight_decay(self,weight_decay):
        self.weight_decay=weight_decay
        self.set_optimizer(self.optimizer_string)
 
    def unlock_backbone(self,unlock_batchnorm=False):
        self.unlock_backbone_partial(-1,unlock_batchnorm=unlock_batchnorm)
    
    def unlock_backbone_partial(self,n_layers_to_unlock, unlock_batchnorm=False):
        unlocked_layers=0
        if self.multi_gpu:
            for name, param in self.model.module.backbone.named_parameters():
                if not isinstance(param, nn.BatchNorm2d) or unlock_batchnorm:
                    param.requires_grad = True
                    unlocked_layers+=1
                if unlocked_layers>=n_layers_to_unlock and n_layers_to_unlock>=0:
                    break
            n_params_old=len(self.params)
            self.params = [p for p in self.model.module.parameters() if p.requires_grad]
        else:
            for name, param in self.model.backbone.named_parameters():
                if not isinstance(param, nn.BatchNorm2d) or unlock_batchnorm:
                    param.requires_grad = True
                    unlocked_layers+=1
                if unlocked_layers>=n_layers_to_unlock and n_layers_to_unlock>=0:
                    break
            n_params_old=len(self.params)
            self.params = [p for p in self.model.parameters() if p.requires_grad]
        print(f"former number of params:{str(n_params_old)} - new number of params : {str(len(self.params))} - unlocked layers : {unlocked_layers}")
        self.optimizer,self.optimizer_descriptor=get_optimizer(self.cfg,self.params)    
        
    def lock_backbone(self):
        if self.multi_gpu:
            for param in self.model.module.backbone.parameters():
                param.requires_grad=False
            self.params = [p for p in self.model.module.parameters() if p.requires_grad]
        else:
            for param in self.model.backbone.parameters():
                param.requires_grad=False
            self.params = [p for p in self.model.parameters() if p.requires_grad]
        self.optimizer,self.optimizer_descriptor=get_optimizer(self.cfg,self.params)
    
    def infer_training_dataset(self,score_thresh=0):
        #dataset_loader=create_train_loader(self.train_dataset,num_workers=self.num_workers,batch_size=self.inference_batch_size)
        self.model.eval()
        output=self.infer_dataset(self.train_loader,return_images=True,score_thresh=score_thresh)
        self.model.train()
        return output
    def infer_valid_dataset(self,score_thresh=0):
        #dataset_loader=create_valid_loader(self.valid_dataset,num_workers=self.num_workers,batch_size=self.inference_batch_size)
        self.model.eval()
        output=self.infer_dataset(self.valid_loader,return_images=True,score_thresh=score_thresh)
        self.model.train()
        return output
    def infer_test_dataset(self,score_thresh=0):
        #dataset_loader=create_test_loader(self.test_dataset,num_workers=self.num_workers,batch_size=self.inference_batch_size)
        self.model.eval()
        output=self.infer_dataset(self.test_loader,return_images=True,score_thresh=score_thresh)
        self.model.train()
        return output
    
    
    ## predicts the bounding boxes using the current model
    ## returns a list of outputs of the model, as well as the target list  
    def infer_dataset(self,dataset_loader,return_images=False,score_thresh=0):
        print("inferring")
        prog_bar = tqdm(dataset_loader, total=len(dataset_loader))
        all_outputs=[]
        all_targets=[]
        all_images=[]
        #raise Exception(f"device used fo inferrence is {self.device}\nmodel device is {self.model.device}")
        #print(f"device used fo inferrence is {self.device}\nmodel device is {self.model.device}")
        for i, data in enumerate(prog_bar):
            #print(f"device used fo inferrence is {self.device}\nmodel device is {self.model.device}")
            images, targets, original_images, label_dicts = data
            #print(f"len of images and original images in infer_dataset {len(images)} - {len(original_images)}")
            #print(f"type of first element in images and original images in infer_dataset {type(images[0])} - {type(original_images[0])}")
            images = list(image.to(self.device) for image in images)              
            images=torch.stack(images,dim=0).to(self.device)
            #raise Exception(f"device used fo inferrence is {self.device}\nmodel device is {self.model.device}\n images on device {images.device}")

            #print(f"shape of input tensor {images.shape}")
            with torch.no_grad():
                outputs = self.model(images) # for some reason this does not work on cpu
            #raise Exception(f"device used fo inferrence is {self.device}\nmodel device is {self.model.device}")    
            if self.yolo:
                targets=[t.to('cpu') for t in targets]
                image_outputs=[output.to('cpu') for output in outputs]
            else:   
                targets = [{k: v.to('cpu') for k, v in t.items()} for t in targets]
                image_outputs = [{k: v.to('cpu') for k, v in t.items()} for t in outputs]
            
            if score_thresh!=0:
                self.remove_outputs_under_thresh(image_outputs,score_thresh)
            #n_boxes=[len(out['boxes'])for out in image_outputs]
            #print(f"in infer dataset number of boxes detected : {n_boxes}")
            
            #remove this later -- dirty trick -- or change it into feature that allows the masks to be compressd to save memory -- for this make kernel_size in the nn.functional a class variable of cfg and also change relevant places in organizer that can be found when searching for the start of this comment
            if "mask" in self.cfg.MODEL:
                if "512" in self.cfg.MODEL_NAME:
                    new_image_outputs=[]
                    for output in image_outputs:
                        new_masks=[]
                        masks=output["masks"]
                        #new_masks=nn.functional.avg_pool2d(masks,kernel_size=2)
                        for mask in masks:
                            new_masks.append(nn.functional.avg_pool2d(mask,kernel_size=2))
                        new_masks=torch.stack(new_masks,dim=0)
                        #print(f"shape of new masks passed from trainer is {new_masks.shape}")
                        output["masks"]=new_masks
                        new_image_outputs.append(output)
                    image_outputs=new_image_outputs

            all_outputs+=image_outputs
            all_targets+=targets
            if return_images:
                all_images += original_images
            #prog_bar.set_description(desc="Inferring")
            
        if return_images:
            return all_outputs,all_targets,all_images,label_dicts[0]
        else:
            return all_outputs,all_targets
        
    def remove_outputs_under_thresh(self,outputs,thresh):
        new_outputs=[]
        for output in outputs:
            mask=[]
            if   self.yolo:
                obj_scores=output[:,4]
            else:
                obj_scores=output['scores']
            for obj_score in obj_scores:
                if obj_score>=thresh: mask.append(1)
                else: mask.append(0)
            if self.yolo:
                new_output=output[np.asarray(mask,dtype=np.uint8)]
            else:
                new_output={}
                for key in output:
                    new_output[key]=output[key][np.asarray(mask,dtype=np.uint8)]
            new_outputs.append(new_output)
                    
                    
            
    ## calculates accuracy using intersection over union
    def calculate_accuracy(self,outputs,targets):
        accuracy_averager=Averager()
        for output,target in zip(outputs,targets):
            # carry further only if there are detected boxes
            if len(output['boxes']) != 0:
                boxes = output['boxes'].data.numpy()
                scores = output['scores'].data.numpy()
                labels = output['labels'].data.numpy()
                boxes = boxes[scores >= self.detection_threshold].astype(np.int32)               ## define detection threhold somewhere
                labels=labels[scores >= self.detection_threshold].astype(np.int32)
                true_boxes=target
                for label in set(labels):
                    predicted_boxes=boxes[labels==label]
                    for predicted_box in predicted_boxes:
                        predicted_box_is_true=False
                        for true_box,true_label in zip(true_boxes['boxes'],true_boxes['labels']):
                            if not true_label==label:
                                intersection_over_union_score=intersection_over_union_box(true_box,predicted_box)
                            
                                if intersection_over_union_score>self.intersection_over_union_threshold:
                                    predicted_box_is_true=True
                                
                        if predicted_box_is_true:
                            accuracy_averager.send(1)
                        else:
                            accuracy_averager.send(0)
            else:
                print("No Boxes detected")
        return accuracy_averager.value
        
    # function for running training iterations
    def train_one_epoch(self):
        print('Training')
        print(f"Number of batches in training set : {len(self.train_loader)}")
        start=time.time()
        # initialize tqdm progress bar
        prog_bar = tqdm(self.train_loader, total=len(self.train_loader))
        loss_dict_list=[]
        for i, data in enumerate(prog_bar):
            self.optimizer.zero_grad()
            images, targets, _, _ = data
            images = list(image.to(self.device) for image in images)
            if self.yolo:
                targets=[t.to(self.device) for t in targets]
                targets=torch.stack(targets).to(self.device)
                torch.autograd.set_detect_anomaly(True)
            else:
                targets = [{k: v.to(self.device) for k, v in t.items()} for t in targets]
            #n_boxes_in_targets=[len(target["boxes"]) for target in targets]
            #print(f"Number of Boxes detected : {n_boxes_in_targets}")
            images=torch.stack(images,dim=0).to(self.device)
            loss_dict = self.model(images, targets)
            if self.yolo:
                del loss_dict['total_loss']
            loss_dict_list.append(loss_dict)
            losses = sum(loss for loss in loss_dict.values())
            loss_value = losses.item()
            losses.backward()
            self.optimizer.step()
            if self.yolo:
                torch.autograd.set_detect_anomaly(False)
        
            # update the loss value beside the progress bar for each iteration
            prog_bar.set_description(desc=f"Loss: {loss_value:.4f}")
        # send losses to accumulator for later use    
        seconds_elapsed=time.time()-start 
        #print(f"first entry in loss dict list in train_one_epoch {loss_dict_list[0]}")
        self.train_loss_accumulator.send(loss_dict_list,seconds_elapsed)  
         
        if self.cfg.EVAL_WHILE_TRAINING:
            self.model.eval()
            outputs,targets=self.infer_training_dataset()
            self.model.train()
            self.train_accuracy_list.append(self.calculate_accuracy(outputs,targets))
            
        return loss_dict, self.train_accuracy_list
        
    # function for running validation iterations
    def validate_one_epoch(self):
        print('Validating')
        print(f"Number of batches in validation set : {len(self.valid_loader)}")
        start=time.time()
        # initialize tqdm progress bar
        prog_bar = tqdm(self.valid_loader, total=len(self.valid_loader))
        loss_dict_list=[]
        for i, data in enumerate(prog_bar):
            images, targets, _, _ = data
            
            images = list(image.to(self.device) for image in images)
            if self.yolo:
                targets=[t.to(self.device) for t in targets]
                targets=torch.stack(targets).to(self.device)
                torch.autograd.set_detect_anomaly(True)
            else:
                targets = [{k: v.to(self.device) for k, v in t.items()} for t in targets]
            #n_boxes_in_targets=[len(target["boxes"]) for target in targets]
            #print(f"Number of Boxes detected : {n_boxes_in_targets}")
            images=torch.stack(images,dim=0).to(self.device)
            
            with torch.no_grad():
                loss_dict = self.model(images, targets)
            if self.yolo:
                del loss_dict['total_loss']
            loss_dict_list.append(loss_dict)
            losses = sum(loss for loss in loss_dict.values())
            loss_value = losses.item()
            # update the loss value beside the progress bar for each iteration
            if self.yolo:
                torch.autograd.set_detect_anomaly(False)
            prog_bar.set_description(desc=f"Loss: {loss_value:.4f}")
            
        # send losses to accumulator for later use    
        seconds_elapsed=time.time()-start 
        self.val_loss_accumulator.send(loss_dict_list,seconds_elapsed)  
        
        if self.cfg.EVAL_WHILE_TRAINING:
            self.model.eval()    
            outputs,targets=self.infer_valid_dataset()
            self.model.train()
            self.val_accuracy_list.append(self.calculate_accuracy(outputs,targets)) 

        return loss_dict, self.val_accuracy_list
           
    def train(self,epochs=1):
        for epoch in range(epochs):
            print(f"\nEPOCH {epoch+1} of {epochs}")
            # start timer and carry out training and validation
            start = time.time()
            train_loss,  train_accuracy = self.train_one_epoch()
            print(f"train loss from accumul {self.train_loss_accumulator.last_loss_dict()}")
            
            if self.cfg.EVAL_WHILE_TRAINING:
                print(f"train accuracy is {train_accuracy[-1]}")
                
            val_loss, val_accuracy = self.validate_one_epoch()
            print(f"val loss from accumul {self.val_loss_accumulator.last_loss_dict()}")
            
            if self.cfg.EVAL_WHILE_TRAINING:
                print(f"validation accuracy is {val_accuracy[-1]}")
                
            print(f"Epoch #{epoch+1} train loss: {self.train_loss_accumulator.last_total():.3f}")   
            print(f"Epoch #{epoch+1} validation loss: {self.val_loss_accumulator.last_total():.3f}")   
            end = time.time()
            print(f"Took {((end - start) / 60):.3f} minutes for epoch {epoch}")
            # save the best model till now if we have the least loss in the...
            # ... current epoch
            self.save_best_model(
                self.val_loss_accumulator.last_total(), epoch, self.model, self.optimizer,self.cfg.MODEL_NAME
            )
            # save the current epoch model
            save_model(epoch,self.cfg, self.model, self.optimizer,self.cfg.MODEL_NAME)
            # save loss plot
            save_avg_loss_plot(self.cfg,self.train_loss_accumulator.all_totals(),self.val_loss_accumulator.all_totals(),self.cfg.MODEL_NAME)
            
            self.train_loss_accumulator.save(self.cfg.OUT_DIR)
            self.val_loss_accumulator.save(self.cfg.OUT_DIR)
            
            if self.cfg.EVAL_WHILE_TRAINING:
                save_accuracy_plot(self.cfg.OUT_DIR,train_accuracy,val_accuracy,self.cfg.MODEL_NAME)
                # start the training epochs
                
            


        
class DescriptionObjectDetectionTrainer(ObjectDetectionTrainer):
    def __init__(
        self,
        cfg,
        ):
        super().__init__(
            cfg,
            )
        self.description_dropout=cfg.DESCRIPTION_DROPOUT
        self.tokenizer=DescriptionTokenizer(BertTokenizer.from_pretrained('bert-base-uncased'),cfg.DEVICE)
        
    def apply_description_dropout(self,descriptions):
        if self.cfg.DESCRIPTION_DROPOUT<=0:
            return descriptions
        dropout_descriptions=[]        
        for d in descriptions:
            if random.random() < self.cfg.DESCRIPTION_DROPOUT: d=""
            dropout_descriptions.append(d)
        return dropout_descriptions
    
    def train_one_epoch(self):
        print('Training')
        start=time.time()
        # initialize tqdm progress bar
        prog_bar = tqdm(self.train_loader, total=len(self.train_loader))
        loss_dict_list=[]
        for i, data in enumerate(prog_bar):
            self.optimizer.zero_grad()
            images, targets, descriptions, _, _ = data
            images = list(image.to(self.device) for image in images)
            targets = [{k: v.to(self.device) for k, v in t.items()} for t in targets]
            images=torch.stack(images,dim=0).to(self.device)
            descriptions=self.apply_description_dropout(descriptions)
            #print(descriptions)
            #descriptions=[d.to(self.device) for d in descriptions] moving to device is instead done in the model
            descriptions, attention_mask= self.tokenizer.tokenize(descriptions)
            inputs={'images':images,'descriptions':descriptions,'attention_mask':attention_mask}
            loss_dict = self.model(inputs, targets)
            loss_dict_list.append(loss_dict)
            losses = sum(loss for loss in loss_dict.values())
            loss_value = losses.item()
            losses.backward()
            self.optimizer.step()        
            # update the loss value beside the progress bar for each iteration
            prog_bar.set_description(desc=f"Loss: {loss_value:.4f}")
            
        # send losses to accumulator for later use    
        seconds_elapsed=time.time()-start 
        self.train_loss_accumulator.send(loss_dict_list,seconds_elapsed)  
            
        if self.cfg.EVAL_WHILE_TRAINING:    
            self.model.eval()
            outputs,targets=self.infer_training_dataset()
            self.model.train()
            self.train_accuracy_list.append(self.calculate_accuracy(outputs,targets))  
        return loss_dict, self.train_accuracy_list        
    
    def validate_one_epoch(self):
        print('Validating')
        start=time.time()
        # initialize tqdm progress bar
        prog_bar = tqdm(self.valid_loader, total=len(self.valid_loader))
        loss_dict_list=[]        
        for i, data in enumerate(prog_bar):
            images, targets, descriptions, _, _ = data
            
            images = list(image.to(self.device) for image in images)
            targets = [{k: v.to(self.device) for k, v in t.items()} for t in targets]
            images=torch.stack(images,dim=0).to(self.device)  
            descriptions=self.apply_description_dropout(descriptions)
            #descriptions=[d.to(self.device) for d in descriptions] moving to device is instead done in the model
            descriptions,attention_mask= self.tokenizer.tokenize(descriptions)
            inputs={'images':images,'descriptions':descriptions,'attention_mask':attention_mask}
            with torch.no_grad():
                loss_dict = self.model(inputs, targets,)
            loss_dict_list.append(loss_dict)
            losses = sum(loss for loss in loss_dict.values())
            loss_value = losses.item()
            # update the loss value beside the progress bar for each iteration
            prog_bar.set_description(desc=f"Loss: {loss_value:.4f}")
            
        # send losses to accumulator for later use    
        seconds_elapsed=time.time()-start 
        self.val_loss_accumulator.send(loss_dict_list,seconds_elapsed)  
            
        if self.cfg.EVAL_WHILE_TRAINING: 
            self.model.eval()    
            outputs,targets=self.infer_valid_dataset()
            self.model.train()
            self.val_accuracy_list.append(self.calculate_accuracy(outputs,targets)) 
            
        return loss_dict, self.val_accuracy_list
    
    def infer_dataset(self,dataset_loader,return_images=False,score_thresh=0):
        print("inferring")
        prog_bar = tqdm(dataset_loader, total=len(dataset_loader))
        all_outputs=[]
        all_targets=[]
        all_images = []
        for i, data in enumerate(prog_bar):
            images, targets, descriptions, original_images, label_dicts = data
            #print(f"len of images and original images in infer_dataset {len(images)} - {len(original_images)} - len description {len(descriptions)}")
            #print(f"type of first element in images and original images in infer_dataset {type(images[0])} - {type(original_images[0])}")
            images = list(image.to(self.device) for image in images)     
            targets = [{k: v.to('cpu') for k, v in t.items()} for t in targets]
            images=torch.stack(images,dim=0).to(self.device)
            descriptions=self.apply_description_dropout(descriptions)
            #descriptions=[d.to(self.device) for d in descriptions] moving to device is instead done in the model
            descriptions,attention_mask= self.tokenizer.tokenize(descriptions)
            #print(f"len descriptions {len(descriptions)}")
            inputs={'images':images,'descriptions':descriptions,'attention_mask':attention_mask}
            #print(f"type of model {type(self.model)}")
            with torch.no_grad():
                outputs = self.model(inputs)
            #print(f"len outputs in infer_dataset{len(outputs)}")
            image_outputs = [{k: v.to('cpu') for k, v in t.items()} for t in outputs]
            
            if score_thresh!=0:
                self.remove_outputs_under_thresh(image_outputs,score_thresh)
            
            #remove this later -- dirty trick -- or change it into feature that allows the masks to be compressd to save memory -- for this make kernel_size in the nn.functional a class variable of cfg and also change relevant places in organizer that can be found when searching for the start of this comment
            if "mask" in self.cfg.MODEL:
                if "512" in self.cfg.MODEL_NAME:
                    new_image_outputs=[]
                    for output in image_outputs:
                        new_masks=[]
                        masks=output["masks"]
                        #new_masks=nn.functional.avg_pool2d(masks,kernel_size=2)
                        for mask in masks:
                            new_masks.append(nn.functional.avg_pool2d(mask,kernel_size=2))
                        new_masks=torch.stack(new_masks,dim=0)
                        #print(f"shape of new masks passed from trainer is {new_masks.shape}")
                        output["masks"]=new_masks
                        new_image_outputs.append(output)
                    image_outputs=new_image_outputs    
            all_outputs+=(image_outputs)
            all_targets+=(targets)
            if return_images:
                all_images += original_images
            #prog_bar.set_description(desc="Inferring")
            
        if return_images:
            #print(f"len of outputs and all_images in infer_dataset {len(all_outputs)} - {len(all_images)}")
            return all_outputs,all_targets,all_images,label_dicts[0]
        else:
            return all_outputs,all_targets
    

