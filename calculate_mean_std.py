import torch
from datasets import create_dataset,create_loader,AllObjectsDataset
from custom_utils import get_train_transform,get_valid_transform
from config import RESIZE_TO
from tqdm.auto import tqdm
TRAIN_VAL_TEST_SPLIT=[1,0,0]
# training images split dict
TRAIN_SPLIT={'mode':'train',
             'split':TRAIN_VAL_TEST_SPLIT}


first_dataset=AllObjectsDataset(TRAIN_SPLIT, RESIZE_TO, RESIZE_TO,  get_train_transform(), load_dataset=False, dataset_name="")
first_loader=create_loader(first_dataset,batch_size=128)



prog_bar = tqdm(first_loader, total=len(first_loader))
total_images=0
total_mean=Nonetotal_std=None
for i, data in enumerate(prog_bar):
    images, targets = data
    images = list(image for image in images)
    targets = [{k: v for k, v in t.items()} for t in targets]
    images=torch.stack(images,dim=0)
    N, C, W, H = images.shape
    total_images+=N
    images_flat=images.view(N, C, -1)
    mean = torch.sum(torch.mean(images_flat, dim=2),dim=0)
    
    std = torch.sum(torch.std(images_flat, dim=2),dim=0)
    if total_mean==None:
        total_mean=mean
        total_std=std
    else:
        total_mean+=mean
        total_std+=std
    # update the loss value beside the progress bar for each iteration

second_dataset=AllObjectsDataset(TRAIN_SPLIT, RESIZE_TO, RESIZE_TO,  get_valid_transform(), load_dataset=False, dataset_name="")
second_loader=create_loader(second_dataset,batch_size=128)
prog_bar_2=tqdm(second_loader, total=len(second_loader))

for i, data in enumerate(prog_bar_2):
    images, targets = data
    images = list(image for image in images)
    targets = [{k: v for k, v in t.items()} for t in targets]
    images=torch.stack(images,dim=0)
    N, C, W, H = images.shape
    total_images+=N
    images_flat=images.view(N, C, -1)
    mean = torch.sum(torch.mean(images_flat, dim=2),dim=0)
    std = torch.sum(torch.std(images_flat, dim=2),dim=0)
    total_mean+=mean
    total_std+=std
    
total_mean/=total_images
total_std/=total_images
print(f"total mean is {total_mean}, total std is {total_std}")