import albumentations as A
import cv2
import numpy as np
import torch
import matplotlib.pyplot as plt
from albumentations.pytorch import ToTensorV2
import json
import sys
sys.path.append('..')

plt.style.use('ggplot')



class LossAccumulator:
    def __init__(self,mode="train",model_name="default_model_name"):
        self.mode=mode
        self.model_name=model_name
        self.epoch=0
        self.cumulative_loss_dict={}
    def __len__(self):
        return self.epoch
    def calculate_average_over_epoch(self,loss_dict_list):
        loss_dict=loss_dict_list[-1]
        total_loss:float=0
        for key in loss_dict.keys():
            for i in range(len(loss_dict_list)-1):            
                loss_dict[key]+=loss_dict_list[i][key]
            loss_dict[key]=float(loss_dict[key]/len(loss_dict_list))
            total_loss+=loss_dict[key]
        return loss_dict,total_loss
    # expects a list of dicts returned by the model at each iteration in the epoch, as well as the time needed for training the epoch
    def send(self,loss_dict_list,seconds_elapsed):
        self.epoch+=1
        loss_dict,total_loss=self.calculate_average_over_epoch(loss_dict_list)
        complete_dict={"loss_dict":loss_dict,"total_loss":total_loss,"seconds_elapsed":seconds_elapsed}
        self.cumulative_loss_dict["epoch "+str(self.epoch)]=complete_dict
        
        
    def get(self):
        return self.cumulative_loss_dict
    
    def last_time(self):
        return self.cumulative_loss_dict["epoch "+str(self.epoch)]["seconds_elapsed"]
    def all_times(self):
        times=[]
        for i in range(self.epoch):
            times.append(self.cumulative_loss_dict["epoch "+str(i+1)]["seconds_elapsed"])
        return times
    def total_time(self):
        return sum(self.all_times)
    
    def last_total(self):
        return self.cumulative_loss_dict["epoch "+str(self.epoch)]["total_loss"]
    def all_totals(self):
        totals=[]
        for i in range(self.epoch):
            totals.append(self.cumulative_loss_dict["epoch "+str(i+1)]["total_loss"])
        return totals
        
    def last_loss_dict(self):
        return self.cumulative_loss_dict["epoch "+str(self.epoch)]["loss_dict"]
    def all_loss_dicts(self):
        loss_dicts=[]
        for i in range(self.epoch):
            loss_dicts.append(self.cumulative_loss_dict["epoch "+str(i+1)]["loss_dict"])
        return loss_dicts
    
    def save(self,directory):
        with open(f"{directory}losses/{self.model_name}_complete_{self.mode}_losses.json","w") as f:
            json.dump(self.cumulative_loss_dict,f)
        
    

# this class keeps track of the training and validation loss values...
# ... and helps to get the average for each epoch as well
class Averager:
    def __init__(self):
        self.current_total = 0.0
        self.iterations = 0.0
        
    def send(self, value):
        self.current_total += value
        self.iterations += 1
    
    @property
    def value(self):
        if self.iterations == 0:
            return 0
        else:
            return 1.0 * self.current_total / self.iterations
    
    def reset(self):
        self.current_total = 0.0
        self.iterations = 0.0
class SaveBestModel:
    """
    Class to save the best model while training. If the current epoch's 
    validation loss is less than the previous least less, then save the
    model state.
    """
    def __init__(
        self,cfg, best_valid_loss=float('inf')
    ):
        self.best_valid_loss = best_valid_loss
        self.cfg=cfg
        
    def __call__(
        self, current_valid_loss, 
        epoch, model, optimizer,model_name
    ):
        if current_valid_loss < self.best_valid_loss:
            self.best_valid_loss = current_valid_loss
            print(f"\nBest validation loss: {self.best_valid_loss}")
            print(f"\nSaving best model for epoch: {epoch+1}\n")
            torch.save({
                'epoch': epoch+1,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                }, self.cfg.MODELS_DIR+model_name+'_best_model.pth')

def intersection_over_union_box(box1,box2):
    # determine the (x, y)-coordinates of the intersection rectangle
    xmin = max(box1[0], box2[0])
    ymin = max(box1[1], box2[1])
    xmax = min(box1[2], box2[2])
    ymax = min(box1[3], box2[3])
    if xmin>=xmax or ymin>=ymax:
        return 0
    intersection=(xmax-xmin)*(ymax-ymin)
    box1_area=(box1[2]-box1[0])*(box1[3]-box1[1])
    box2_area=(box2[2]-box2[0])*(box2[3]-box2[1])
    union=box1_area+box2_area-intersection
    return intersection/union
            
def collate_fn(batch):
    """
    To handle the data loading as different images may have different number 
    of objects and to handle varying size tensors as well.
    """
    return tuple(zip(*batch))
# define the training tranforms
def get_train_transform():
    print("start train transform")
    out=A.Compose([
        A.Flip(0.5),
        A.RandomRotate90(0.5),
        A.MotionBlur(p=0.2),
        A.MedianBlur(blur_limit=3, p=0.1),
        A.Blur(blur_limit=3, p=0.1),

        #A.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]), # this was added to every transform, it seems resnet50 expects this normailzation
        ToTensorV2(p=1.0),
    ], bbox_params={
        'format': 'pascal_voc',
        'label_fields': ['labels']
    })
    print("fin train transform")
    return out
# define the validation transforms
def get_valid_transform():
    print("start val transform")
    out=A.Compose([
        #A.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ToTensorV2(p=1.0),
    ], bbox_params={
        'format': 'pascal_voc', 
        'label_fields': ['labels']
    })
    print("fin val transform")
    return out
# define the test transforms
def get_test_transform():
    return A.Compose([
        #A.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ToTensorV2(p=1.0),
    ], bbox_params={
        'format': 'pascal_voc', 
        'label_fields': ['labels']
    })

    
        
'''def show_tranformed_image(train_loader):
    """
    This function shows the transformed images from the `train_loader`.
    Helps to check whether the tranformed images along with the corresponding
    labels are correct or not.
    Only runs if `VISUALIZE_TRANSFORMED_IMAGES = True` in config.py.
    """
    if len(train_loader) > 0:
        for i in range(1):
            images, targets = next(iter(train_loader))
            image_name="Transformed-image"+targets['image_id']
            images = list(image.to(DEVICE) for image in images)
            targets = [{k: v.to(DEVICE) for k, v in t.items()} for t in targets]
            boxes = targets[i]['boxes'].cpu().numpy().astype(np.int32)
            labels = targets[i]['labels'].cpu().numpy().astype(np.int32)
            sample = images[i].permute(1, 2, 0).cpu().numpy()
            for box_num, box in enumerate(boxes):
                cv2.rectangle(sample,
                            (box[0], box[1]),
                            (box[2], box[3]),
                            (0, 0, 255), 2)
                cv2.putText(sample, CLASSES[labels[box_num]], 
                            (box[0], box[1]-10), cv2.FONT_HERSHEY_SIMPLEX, 
                            1.0, (0, 0, 255), 2)
            
            cv2.imwrite(person_detection_output_path+image_name, sample)'''
            
            
def save_model(epoch,cfg, model, optimizer, model_name):
    """
    Function to save the trained model till current epoch, or whenver called
    """
    torch.save({
                'epoch': epoch+1,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                }, cfg.MODELS_DIR+model_name+'_last_model.pth')
    
'''def save_loss_plot(OUT_DIR, train_loss, val_loss,model_name):
    figure_1, train_ax = plt.subplots()
    figure_2, valid_ax = plt.subplots()
    train_ax.plot(train_loss, color='tab:blue',label="training loss")
    train_ax.set_xlabel('iterations')
    train_ax.set_ylabel('train loss')
    train_ax.set_ylim(bottom=0)
    valid_ax.plot(val_loss, color='tab:red',label="validation loss")
    valid_ax.set_xlabel('iterations')
    valid_ax.set_ylabel('validation loss')
    valid_ax.set_ylim(bottom=0)
    figure_1.savefig(f"{OUT_DIR}/plots/{model_name}_train_loss.png")
    figure_2.savefig(f"{OUT_DIR}/plots/{model_name}_valid_loss.png")
    print('SAVING LOSS PLOTS COMPLETE...')
    plt.close('all')'''
    
def save_avg_loss_plot(cfg, avg_train_loss, avg_val_loss,model_name):
    figure_1, train_ax = plt.subplots()
    train_ax.plot(avg_train_loss, color='tab:blue',label="training loss")
    train_ax.plot(avg_val_loss, color='tab:red',label="validation loss")
    train_ax.set_xlabel('epoch')
    train_ax.set_ylabel('loss')
    train_ax.set_ylim(bottom=0)
    train_ax.legend()
    figure_1.savefig(f"{cfg.PLOT_DIR}{model_name}_epochs_loss.png")
    print('SAVING AVERAGE LOSS PLOT COMPLETE...')
    plt.close('all')
    
def save_accuracy_plot(cfg, train_accuracy, val_accuracy, model_name):
    figure_1, train_ax = plt.subplots()
    train_ax.plot(train_accuracy, color='tab:blue', label="training accuracy")
    train_ax.plot(val_accuracy, color='tab:red', label="validation accuracy")
    train_ax.set_xlabel('epoch')
    train_ax.set_ylabel('accuracy ')
    train_ax.set_ylim(bottom=0,top=1)
    train_ax.legend()
    figure_1.savefig(f"{cfg.PLOT_DIR}/plots/accuracy_{model_name}.png")
    print('SAVING ACCURACY PLOT COMPLETE...')
    plt.close('all')
