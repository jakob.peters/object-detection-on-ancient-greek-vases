import os
import cv2
import sys
import numpy as np
sys.path.append('..')
import json
import base64
import glob as glob
from config import Cfg
annotationpath="/scratch/users/jpeters4/Annotationen/"
        
imagepath="/scratch/users/jpeters4/Annotationen/images/"
jsonpath="/scratch/users/jpeters4/Annotationen/jsonfiles/"  
outpath="/scratch/users/jpeters4/Annotationen/person_detection_outputs/"
#construct the output folder system
cfg=Cfg()
cfg.OUT_DIR = outpath
cfg.IMAGE_DIR=imagepath
cfg.JSON_DIR=jsonpath 
cfg.check_cfg()

if not os.path.exists(imagepath) or not os.path.isdir(imagepath):
    os.makedirs(imagepath)
    print(f"created folder {imagepath}")
    
if not os.path.exists(jsonpath) or not os.path.isdir(jsonpath):
    os.makedirs(jsonpath)
    print(f"created folder {jsonpath}")

def load_cv2_from_string(s):
    img_data=base64.b64decode(s)
    img_array=np.frombuffer(img_data,np.uint8)
    image=cv2.imdecode(img_array,cv2.IMREAD_COLOR)
    return image
    
def unify_image_type(image):
    if (image.shape[2]==1):
        image=cv2.cvtColor(image,cv2.COLOR_GRAY2RGB).astype(np.float32)
    elif(image.shape[2]==4):
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB).astype(np.float32)
    elif(image.shape[2]==3):
        image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB).astype(np.float32)
    return image

file_paths = glob.glob(f"{annotationpath}/*")  
for file_path in file_paths:
    if file_path.split('.')[-1]=='json':
        try:
            with open(file_path,'r') as f:
                data=json.load(f)
            image=load_cv2_from_string(data['imageData'])
            image=unify_image_type(image)
            if 'imageData' in data:
                del data['imageData']
            cv2.imwrite(imagepath+file_path.split(os.path.sep)[-1][:-5]+'.jpg', image)
            with open (jsonpath+file_path.split(os.path.sep)[-1],'w') as f:
                json.dump(data,f)
            print("successful for :")
            print(jsonpath+file_path.split(os.path.sep)[-1])
            print(imagepath+file_path.split(os.path.sep)[-1][:-5]+'.jpg')
        except Exception as e:
            print(e)
            continue
        



