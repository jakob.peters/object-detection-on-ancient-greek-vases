from yolox.models.yolox import YOLOX
from yolox.models.yolo_pafpn import YOLOPAFPN
from yolox.models.yolo_head import YOLOXHead
from torchvision.models.detection.backbone_utils import resnet_fpn_backbone
from yolox.utils import bboxes_iou,  meshgrid 
import torch
from loguru import logger
from yolox.utils import bboxes_iou
import torch.nn.functional as F
class Resnet50fpnYOLOX(YOLOX):
    def __init__(self,cfg,backbone=None,head=None,num_classes=80):
        super().__init__(num_classes)#backbone=resnet_fpn_backbone('resnet50', pretrained=True),head=YOLOXHead(num_classes,strides=[4, 8, 16, 32],in_channels=[256, 256, 256, 256]))
        #backbone_strides=[4, 8, 16, 32, 64]
        #backbone_in_channels=[256, 512, 1024, 2048, 2048]
        self.cfg=cfg
        if backbone is None:
            if cfg.YOLO_USE_RESNET_BACKBONE:
                backbone=resnet_fpn_backbone('resnet50', pretrained=True)
                self.backbone_str="resnet50"
            else:
                backbone = YOLOPAFPN()
                self.backbone_str="yolopafpn"
        if head is None:
            if cfg.YOLO_USE_RESNET_BACKBONE:
                head=MyYOLOXHead(num_classes,strides=[4, 8, 16, 32, 64],in_channels=[256, 256, 256, 256, 256])
            else:
                head = MyYOLOXHead(num_classes)
        self.backbone=backbone
        self.head=head
        
    def forward(self,x,targets=None):
        # fpn output content features of [dark3, dark4, dark5]
        fpn_outs = self.backbone(x)
        if self.backbone_str!="yolopafpn":
            #del fpn_outs['pool']
            fpn_outs_list=[fpn_outs[key]for key in fpn_outs.keys()]
            fpn_outs=fpn_outs_list
        if self.training:
            assert targets is not None
            loss, iou_loss, conf_loss, cls_loss, l1_loss, num_fg = self.head(
                fpn_outs, targets, x
            )
            outputs = {
                "total_loss": loss,
                "iou_loss": iou_loss,
                "l1_loss": l1_loss,
                "conf_loss": conf_loss,
                "cls_loss": cls_loss,
                "num_fg": num_fg,
            }
        else:
            outputs = self.head(fpn_outs)

        return outputs
        
        
        
class MyYOLOXHead(YOLOXHead):
    def __init__(self,num_classes,width=1.0,strides=[8, 16, 32],in_channels=[256, 512, 1024],act="silu",depthwise=False,):
        super().__init__(num_classes,width=width,strides=strides,in_channels=in_channels,act=act,depthwise=depthwise)
        print(f"num classes is {num_classes}")
        
        
    def get_output_and_grid(self, output, k, stride, dtype):
        grid = self.grids[k]

        batch_size = output.shape[0]
        n_ch = 5 + self.num_classes
        #print(f"shape of output in get_output_and_grid {output.shape}")
        #print(f"part of shape for hsize,wsize {output.shape[-2:]}")
        hsize, wsize = output.shape[-2:]
        if grid.shape[2:4] != output.shape[2:4]:
            #print(f"grid shape and output shape not equal")
            yv, xv = meshgrid([torch.arange(hsize), torch.arange(wsize)])
            #print(f"xv shape  is {xv.shape}")
            #print(f"xv is {xv}")
            #print(f"yv is {yv}")
            grid = torch.stack((xv, yv), 2).view(1, 1, hsize, wsize, 2).type(dtype)
            self.grids[k] = grid
            #print(f"grid shape is {grid.shape}")
        #else:
            #print(f"grid shape and output shape equal")
        #print(f"shape of output before making changes in get_output_and_grid {output.shape}")
        output = output.view(batch_size, 1, n_ch, hsize, wsize)
        output = output.permute(0, 1, 3, 4, 2).reshape(
            batch_size, hsize * wsize, -1
        )
        grid = grid.view(1, -1, 2)
        output[..., :2] = (output[..., :2] + grid) * stride
        output[..., 2:4] = torch.exp(output[..., 2:4]) * stride
        #print(f"output shape before returning from get_output_and_grid {output.shape}")
        return output, grid
