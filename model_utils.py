from model_exceptions import *        
from model import MaskRCNNDescriptionModel,FasterRCNNDescriptionModel,MyFasterRCNN,MyMaskRCNN
from yolox_model import Resnet50fpnYOLOX
import torch.nn as nn
import torch
from torch.nn.utils.rnn import pad_sequence
def create_model(cfg):
    num_classes=cfg.NUM_CLASSES
    if cfg.MODEL=="maskrcnn":
        do_masks=True
        yolo=False
    elif cfg.MODEL=="yolo":
        do_masks=False
        yolo=True
    else:
        yolo=False
        do_masks=False
    if cfg.DATASET=="alldescriptions" or cfg.DATASET=="persondescriptions":
        if yolo:
            raise YoloCantUseDescriptionsException
        if do_masks:
            model=MaskRCNNDescriptionModel(
                cfg,
                num_classes=num_classes,
                # transform parameters
                min_size=800,
                max_size=1333,
                image_mean=None,
                image_std=None,
                # RPN parameters
                rpn_anchor_generator=None,
                rpn_head=None,
                rpn_pre_nms_top_n_train=2000,
                rpn_pre_nms_top_n_test=1000,
                rpn_post_nms_top_n_train=2000,
                rpn_post_nms_top_n_test=1000,
                rpn_nms_thresh=cfg.IOU_NMS_THRESH,
                rpn_fg_iou_thresh=cfg.RPN_FG_IOU_THRESH,
                rpn_bg_iou_thresh=cfg.RPN_BG_IOU_THRESH,
                rpn_batch_size_per_image=256,
                rpn_positive_fraction=0.5,
                rpn_score_thresh=0.0,
                # Box parameters
                box_roi_pool=None,
                box_head=None,
                box_predictor=None,
                box_fg_iou_thresh=cfg.BOX_FG_IOU_THRESH,
                box_bg_iou_thresh=cfg.BOX_BG_IOU_THRESH,
                box_batch_size_per_image=512,
                box_positive_fraction=0.25,
                bbox_reg_weights=None,
                freeze_backbone=True,
                roi_pooling_output_size=cfg.ROI_POOLING_OUTPUT_SIZE   
            )
        else:
            model=FasterRCNNDescriptionModel(
                cfg,
                num_classes=num_classes,
                # transform parameters
                min_size=800,
                max_size=1333,
                image_mean=None,
                image_std=None,
                # RPN parameters
                rpn_anchor_generator=None,
                rpn_head=None,
                rpn_pre_nms_top_n_train=2000,
                rpn_pre_nms_top_n_test=1000,
                rpn_post_nms_top_n_train=2000,
                rpn_post_nms_top_n_test=1000,
                rpn_nms_thresh=cfg.IOU_NMS_THRESH,
                rpn_fg_iou_thresh=cfg.RPN_FG_IOU_THRESH,
                rpn_bg_iou_thresh=cfg.RPN_BG_IOU_THRESH,
                rpn_batch_size_per_image=256,
                rpn_positive_fraction=0.5,
                rpn_score_thresh=0.0,
                # Box parameters
                box_roi_pool=None,
                box_head=None,
                box_predictor=None,
                box_score_thresh=0.05,
                box_nms_thresh=0.5,
                box_detections_per_img=100,
                box_fg_iou_thresh=cfg.BOX_FG_IOU_THRESH,
                box_bg_iou_thresh=cfg.BOX_BG_IOU_THRESH,
                box_batch_size_per_image=512,
                box_positive_fraction=0.25,
                bbox_reg_weights=None,
                freeze_backbone=True,
                roi_pooling_output_size=cfg.ROI_POOLING_OUTPUT_SIZE       
            )
            
        model.device=cfg.DEVICE
    else:
        if do_masks:
            if yolo:
                raise YoloCantUseMasksException
            model=MyMaskRCNN(
                cfg,
                num_classes=num_classes,
                # transform parameters
                min_size=800,
                max_size=1333,
                image_mean=None,
                image_std=None,
                # RPN parameters
                rpn_anchor_generator=None,
                rpn_head=None,
                rpn_pre_nms_top_n_train=2000,
                rpn_pre_nms_top_n_test=1000,
                rpn_post_nms_top_n_train=2000,
                rpn_post_nms_top_n_test=1000,
                rpn_nms_thresh=cfg.IOU_NMS_THRESH,
                rpn_fg_iou_thresh=cfg.RPN_FG_IOU_THRESH,
                rpn_bg_iou_thresh=cfg.RPN_BG_IOU_THRESH,
                rpn_batch_size_per_image=256,
                rpn_positive_fraction=0.5,
                rpn_score_thresh=0.0,
                # Box parameters
                box_roi_pool=None,
                box_head=None,
                box_predictor=None,
                box_score_thresh=0.05,
                box_nms_thresh=0.5,
                box_detections_per_img=100,
                box_fg_iou_thresh=cfg.BOX_FG_IOU_THRESH,
                box_bg_iou_thresh=cfg.BOX_BG_IOU_THRESH,
                box_batch_size_per_image=512,
                box_positive_fraction=0.25,
                bbox_reg_weights=None,
                freeze_backbone=True,  
                roi_pooling_output_size=cfg.ROI_POOLING_OUTPUT_SIZE      
            )
        else:
            if yolo:
                model=Resnet50fpnYOLOX(cfg,num_classes=num_classes)
            else:
                model=MyFasterRCNN(
                    cfg,
                    num_classes=num_classes,
                    # transform parameters
                    min_size=800,
                    max_size=1333,
                    image_mean=None,
                    image_std=None,
                    # RPN parameters
                    rpn_anchor_generator=None,
                    rpn_head=None,
                    rpn_pre_nms_top_n_train=2000,
                    rpn_pre_nms_top_n_test=1000,
                    rpn_post_nms_top_n_train=2000,
                    rpn_post_nms_top_n_test=1000,
                    rpn_nms_thresh=cfg.IOU_NMS_THRESH,
                    rpn_fg_iou_thresh=cfg.RPN_FG_IOU_THRESH,
                    rpn_bg_iou_thresh=cfg.RPN_FG_IOU_THRESH,
                    rpn_batch_size_per_image=256,
                    rpn_positive_fraction=0.5,
                    rpn_score_thresh=0.0,
                    # Box parameters
                    box_roi_pool=None,
                    box_head=None,
                    box_predictor=None,
                    box_score_thresh=0.05,
                    box_nms_thresh=0.5,
                    box_detections_per_img=100,
                    box_fg_iou_thresh=cfg.BOX_FG_IOU_THRESH,
                    box_bg_iou_thresh=cfg.BOX_BG_IOU_THRESH,
                    box_batch_size_per_image=512,
                    box_positive_fraction=0.25,
                    bbox_reg_weights=None,
                    freeze_backbone=True,  
                    roi_pooling_output_size=cfg.ROI_POOLING_OUTPUT_SIZE      
                )
        model.device=cfg.DEVICE
        
    if cfg.USE_MULTI_GPU:
        print("using multiple gpu")
        model=nn.DataParallel(model)
        model.device=cfg.DEVICE
        return model
        
    else:
        print("using single gpu")
        return model    
    
#creates model as given in model.py,    
def get_model_and_params(cfg):
    model = create_model(cfg)
    model = model.to(cfg.DEVICE)
    params = [p for p in model.parameters() if p.requires_grad]
    print("model built")
    return model,params    

def load_model(cfg):
    saved_dict=torch.load(cfg.LOAD_MODEL_PATH,map_location=cfg.DEVICE)
    model_state_dict=saved_dict['model_state_dict']
    optimizer_state_dict=saved_dict['optimizer_state_dict']
    model = create_model(cfg)
    model = model.to(cfg.DEVICE)
    model.load_state_dict(model_state_dict)
    params = [p for p in model.parameters() if p.requires_grad]
    print("\nmodel loaded from file\n")
    return     model,params,optimizer_state_dict

def load_model_state(cfg):
   model,params,_=load_model(cfg)
    

# creates optimizer for the parameters, hyperparameters for the optimizer are taken from the class variabless
def get_optimizer(cfg,params):
    optimizer_string=cfg.OPTIMIZER_STRING
    valid_optimizer_strings=["SGD","Adam","Adagrad"]
    if optimizer_string=="SGD":
        optimizer=torch.optim.SGD(params, lr=cfg.LR, momentum=cfg.MOMENTUM, weight_decay=cfg.WEIGHT_DECAY)
        optimizer_descriptor=optimizer_string+f'_lr={cfg.LR}_momentum={cfg.MOMENTUM}_weightdecay={cfg.WEIGHT_DECAY}'
        print(f'Set optimizer to {optimizer_descriptor}')
        return optimizer, optimizer_descriptor
    elif optimizer_string=="Adam":
        optimizer=torch.optim.Adam(params, lr=cfg.LR, betas=cfg.BETAS, eps=cfg.EPS, weight_decay=cfg.WEIGHT_DECAY, amsgrad=cfg.AMSGRAD)
        optimizer_descriptor=optimizer_string+f'_lr={cfg.LR}_betas={cfg.BETAS}_eps{cfg.EPS}_weightdecay={cfg.WEIGHT_DECAY}_amsgrad={cfg.AMSGRAD}'
        print(f'Set optimizer to {optimizer_descriptor}')
    elif optimizer_string=="Adagrad":
        optimizer=torch.optim.Adagrad(params, lr=cfg.LR, lr_decay=cfg.LR_DECAY, eps=cfg.EPS, weight_decay=cfg.WEIGHT_DECAY, initial_accumulator_value=cfg.INITIAL_ACCUMULATOR_VALUE)
        optimizer_descriptor=optimizer_string+f'_lr={cfg.LR}_lrdecay={cfg.LR_DECAY}_eps{cfg.EPS}_weightdecay={cfg.WEIGHT_DECAY}_initialaccumulatorvalue={cfg.INITIAL_ACCUMULATOR_VALUE}'       
        print(f'Set optimizer to {optimizer_descriptor}')     
    else:
        raise InvalidOptimizerStringException(f"Invalid optimizer string: {optimizer_string}, valid optimizer strings are {valid_optimizer_strings}")
    return optimizer,optimizer_descriptor
    

    
class DescriptionTokenizer:
    def __init__(self,tokenizer,device):
        self.tokenizer=tokenizer
        self.device=device
        

    def tokenize(self, descriptions):
        encoded_descriptions = [self.tokenizer.encode(description, add_special_tokens=True) for description in descriptions]

        # Use pad_sequence to pad and concatenate the sequences
        padded_descriptions = pad_sequence([torch.tensor(encoded_desc) for encoded_desc in encoded_descriptions], batch_first=True, padding_value=self.tokenizer.pad_token_id)

        # Generate attention masks (1 for real tokens, 0 for padding)
        attention_masks = (padded_descriptions != self.tokenizer.pad_token_id).to(torch.int)

        # Move the tensors to the specified device (e.g., GPU)
        padded_descriptions = padded_descriptions.to(self.device)
        attention_masks = attention_masks.to(self.device)

        return padded_descriptions, attention_masks