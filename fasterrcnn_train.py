from organizer import Organizer
from config import Cfg


cfg=Cfg()

cfg.OUT_DIR = "/scratch/users/jpeters4/Annotationen/person_detection_outputs/"
cfg.IMAGE_DIR="/scratch/users/jpeters4/Annotationen/images/"
cfg.JSON_DIR="/scratch/users/jpeters4/Annotationen/jsonfiles/"  
cfg.CSV_DIR="/scratch/users/jpeters4/csv/"
  
cfg.MODEL="fasterrcnn"
cfg.RESIZE_TO=512
cfg.BATCH_SIZE=12
cfg.NUM_EPOCHS_TRAIN=75
cfg.NUM_EPOCHS_FINETUNE=75
cfg.DATASET="persondescriptions"
cfg.DESCRIPTION_DROPOUT=0.5
cfg.LR=0.00005
cfg.FINETUNE_LR=0.00005
cfg.anchor_size_divideby=[32,16,8,4,2] 
cfg.ASPECT_RATIOS=((0.25, 0.5, 1.0, 2.0, 4.0),)
cfg.YOLO_USE_RESNET_BACKBONE=True
cfg.MODEL_NAME="fasterrcnn_persondescriptiondataset_imsize512_conv2dhead"

print(cfg.MODEL_NAME)
print(f"constructing organizer for {cfg.MODEL_NAME}")
org=Organizer(cfg)
org.train()
org.save_results()

print("finished_training")