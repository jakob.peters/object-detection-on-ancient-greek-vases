class InvalidOptimizerStringException(Exception):
    pass
class InvalidLoaderTypeException(Exception):
    pass
class YoloCantUseDescriptionsException(Exception):
    pass
class YoloCantUseMasksException(Exception):
    pass
