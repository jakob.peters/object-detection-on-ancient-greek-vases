import cv2 
import pickle
import torch
from train import get_trainer
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import copy
import os
import gc
from image_utils import preprocess_image
from model_utils import load_model, get_optimizer, get_model_and_params,DescriptionTokenizer
from transformers import BertTokenizer  
import math

class Organizer:
    def __init__(self,cfg,make_trainer=True):
        self.cfg=cfg
        if make_trainer:
            self.trainer=get_trainer(cfg)
        self.make_trainer=make_trainer
        self.results_dict={
            "cfg":cfg,
            "validation_results":None,
            "test_results":None,
            "train_loss_accumulator":None,
            "validation_loss_accumulator":None,
            "best_hyperparameters":None,
            "test_metrics":None,
            "test_scores":None
        }
        if cfg.MODEL=="maskrcnn":
            self.do_masks=True
            self.yolo=False
        elif cfg.MODEL=="yolo":
            self.do_masks=False
            self.yolo=True
        else:
            self.yolo=False
            self.do_masks=False
    
    def set_results_dict(self,results_dict):
        self.results_dict=results_dict        

    def get_results(self):
        return self.outputs,self.targets,self.images,self.label_dict
    
    def make_result_dict(self,outputs,targets,images,label_dict):
        result_dict={            
            "outputs":outputs,
            "targets":targets,
            "images":images,
            "label_dict":label_dict 
        }
        return result_dict
        
    def save_results(self):
        with open(self.cfg.RESULTS_DIR+self.cfg.MODEL_NAME+'.pkl', 'wb') as file:
            pickle.dump(self.results_dict, file)
            
    def infer_test_dataset(self,score_thresh=0):
        print(f"Inferring Test Dataset")
        outputs,targets,images,label_dict=self.trainer.infer_test_dataset(score_thresh=score_thresh)
        test_result_dict=self.make_result_dict(outputs,targets,images,label_dict)
        return test_result_dict
        self.results_dict["test_results"]=test_result_dict
        
    def infer_valid_dataset(self,score_thresh=0):
        print(f"Inferring validation Dataset")
        outputs,targets,images,label_dict=self.trainer.infer_valid_dataset(score_thresh=score_thresh)
        valid_result_dict=self.make_result_dict(outputs,targets,images,label_dict)
        return valid_result_dict
        self.results_dict["validation_results"]=valid_result_dict
    
    def infer_image_list(self,image_list:list,description_list=None,batch_size=0,score_thresh=0):
        outputs=[]
        if not self.create_model:
            raise Exception("cannot infer images when create_model is False")
        if self.cfg.DATASET in ["alldescriptons","persondescriptions"] and type(description_list)==type(None):
            print("inferring without using descriptions")
            description_list=[]
            for i in range(len(image_list)):
                description_list.append("")
        if self.cfg.DATASET in ["alldescriptons","persondescriptions"] and len(description_list)!=len(image_list):
            raise Exception("image_list and description_list are not of the same length")
        if batch_size!=0:
            n_batches=(len(image_list)//batch_size)+1
            for i in range(n_batches):
                print(f"inferring batch {i} of {max(n_batches,1)}")
                current_image_list=image_list[i:min(len(image_list),(i+1)*batch_size)]
                for i,current_image in enumerate(current_image_list):
                    processed_image,h,w=preprocess_image(current_image,target_height=self.cfg.RESIZE_TO,target_width=self.cfg.RESIZE_TO)
                    processed_image/=255.0
                    current_image_list[i]=torch.from_numpy(processed_image).permute(2,0,1).to(self.cfg.DEVICE)
                images=torch.stack(current_image_list,dim=0).to(self.cfg.DEVICE)
                    
                if self.cfg.DATASET in ["alldescriptons","persondescriptions"]:
                    current_description_list=description_list[i:min(len(image_list),(i+1)*batch_size)]                            
                    descriptions, attention_mask= self.tokenizer.tokenize(current_description_list)
                    inputs={'images':images,'descriptions':descriptions,'attention_mask':attention_mask}
                else:
                    inputs=images
                self.trainer.model.eval()
                with torch.no_grad():
                    output=self.trainer.model(inputs)
                if self.cfg.MODEL=="yolo":
                    output=self.yolo_nms(output)
                self.trainer.model.train()
                output=self.process_outputs(output)
                output=self.apply_score_thresh(output,score_thresh)
                outputs+=output
        else:
            for i,image in enumerate(image_list):
                image,h,w=preprocess_image(image,target_height=self.cfg.RESIZE_TO,target_width=self.cfg.RESIZE_TO)
                image/=255.0
                image=torch.from_numpy(image).permute(2,0,1).to(self.cfg.DEVICE)
                print(f"inferring image {i} of {len(image_list)}")
                images=torch.stack([image],dim=0).to(self.cfg.DEVICE)
                if self.cfg.DATASET in ["alldescriptons","persondescriptions"]:                            
                    descriptions, attention_mask= self.tokenizer.tokenize([description_list[i]])
                    inputs={'images':images,'descriptions':descriptions,'attention_mask':attention_mask}
                else:
                    inputs=images
                self.trainer.model.eval()
                with torch.no_grad():
                    output=self.trainer.model(inputs)
                self.trainer.model.train()
                if self.cfg.MODEL=="yolo":
                    output=self.yolo_nms(output)
                output=self.process_outputs(output)
                output=self.apply_score_thresh(output,score_thresh)
                outputs+=output
        return outputs
    
    
    def process_outputs(self,outputs):
        if self.yolo:
            image_outputs=[self.unify_output(output.to('cpu')) for output in outputs]
        else:   
            image_outputs = [self.unify_output({k: v.to('cpu') for k, v in t.items()}) for t in outputs]
        return image_outputs

            
        
    def calc_performance_and_generate_images(self):
        result_dict=self.infer_valid_dataset()
        score_dict,labels_score_dict,best_hyperparameters=self.search_hyperparameters(result_dict)
        self.results_dict["best_hyperparameters"]=best_hyperparameters
        self.results_dict["validation_scores"]=score_dict
        self.results_dict["per_label_validation_scores"]=labels_score_dict
        score_thresh=best_hyperparameters["best_score_thresh"]
        iou_thresh=best_hyperparameters["best_iou_thresh"]
        mask_thresh=best_hyperparameters["best_mask_thresh"]
        # it is important to delete the masks in validation inference results to free up space
        if self.cfg.MODEL=="maskrcnn":
            for i in range(len(result_dict["targets"])):
                del result_dict["targets"][i]["masks"]
            for i in range(len(result_dict["outputs"])):
                del result_dict["outputs"][i]["masks"]
            #del result_dict
            do_masks=True
        else:
            do_masks=False
        self.results_dict["validation_results"]=result_dict
        del result_dict
        
        result_dict=self.infer_test_dataset(score_thresh=score_thresh)           # pass score thresh to infer_dataset to reduce memory usage
        #result_dict=self.results_dict["test_results"]
        print(f"generating images with predictions for test dataset")
        self.generate_images(result_dict,score_thresh,mask_thresh,max_predicted_boxes_per_image=100)
        outputs=result_dict["outputs"]
        targets=result_dict["targets"]
        label_dict=result_dict['label_dict']
        print(f"calculating metrics for test dataset")
        test_metrics=self.calculate_metrics(outputs,targets,label_dict,score_thresh,mask_thresh,iou_thresh,calc_mask_iou=do_masks) 
        self.results_dict["test_metrics"]=test_metrics
        print(f"calculating recall sensitivity and f1 score for test dataset")
        scores=self.calculate_scores(test_metrics)
        scores['mask_iou']=test_metrics["avg_mask_iou"]
        self.results_dict["test_scores"]=scores
        score_thresh_range=[0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95]
        self.results_dict["per_label_test_scores"]=self.make_and_save_pr_curves_calc_auc(outputs,targets,label_dict,score_thresh_range,iou_thresh,plot_images=False)
        print(f"calculated test scores are {scores}")
        # it is important to delete the masks in test inference results to free up space
        if self.cfg.MODEL=="maskrcnn":
            for i in range(len(result_dict["targets"])):
                del result_dict["targets"][i]["masks"]
            for i in range(len(result_dict["outputs"])):
                del result_dict["outputs"][i]["masks"]
        self.results_dict["test_results"]=result_dict
        self.save_results()
        print("saved results after performance calculation")
        
    def train(self):
        self.trainer.train(epochs=self.cfg.NUM_EPOCHS_TRAIN)
        if self.cfg.NUM_EPOCHS_FINETUNE!=0:
            self.trainer.unlock_backbone()
            self.trainer.set_lr(self.cfg.FINETUNE_LR)
            self.trainer.train(epochs=self.cfg.NUM_EPOCHS_FINETUNE)
            
        self.results_dict["train_loss_accumulator"]=self.trainer.train_loss_accumulator.get()
        self.results_dict["validation_loss_accumulator"]=self.trainer.val_loss_accumulator.get()
        
            
    def infer_list_of_images(self,images):
        self.trainer.model.eval()
        with torch.no_grad():
            outputs=self.trainer.model(images)
        self.trainer.model.train()
        if self.yolo:
            image_outputs=[output.to('cpu') for output in outputs]
            
        else:   
            image_outputs = [{k: v.to('cpu') for k, v in t.items()} for t in outputs]
        return image_outputs
        

    def generate_test_images(self,score_threshold,mask_threshold,max_predicted_boxes_per_image=15):
        print("Generating Images for Test set")
        result_dict=self.results_dict["test_results"]
        self.generate_images(result_dict,score_threshold,mask_threshold,max_predicted_boxes_per_image)

    def generate_valid_images(self,score_threshold,mask_threshold,max_predicted_boxes_per_image=15):
        print("Generating Images for Validation set")
        result_dict=self.results_dict["validation_results"]
        self.generate_images(result_dict,score_threshold,mask_threshold,max_predicted_boxes_per_image)
        
    def generate_images(self,result_dict,score_threshold,mask_threshold,max_predicted_boxes_per_image=15):
        outputs=result_dict["outputs"]
        targets=result_dict["targets"]
        images=result_dict["images"]
        label_dict=result_dict["label_dict"]
        output_directory=self.cfg.PREDICTED_IMAGES_DIR+self.cfg.MODEL_NAME+"/" 
        if not os.path.exists(output_directory) or not os.path.isdir(output_directory):
            os.makedirs(output_directory)
        if self.yolo:
            outputs=self.yolo_nms(outputs)
            max_score=0
            for i,output,target,image in zip(range(len(outputs)),outputs,targets,images):  

                boxes=output[:,:4]
                obj_score=output[:,4]
                cls_outs=output[:,5:]
                
                target_mask=target[:,-1].numpy().astype(int)
                target_boxes=target[:,1:5].numpy()
                target_labels=target[:,0].numpy()
                
                image_np=image
                for t_bbox ,t_label, has_t in zip(target_boxes,target_labels,target_mask):
                    if not has_t: break
                    xcenter,ycenter,width,height = t_bbox
                    xmin,ymin,xmax,ymax=int(xcenter-(width/2)),int(ycenter-(height/2)),int(xcenter+(width/2)),int(ycenter+(height/2))
                    t_label=t_label.astype(int)
                    true_color=(0,0,255)
                    cv2.rectangle(image_np, (xmin,ymin),(xmax,ymax),true_color,2)
                    cv2.putText(image_np,"true_label: "+label_dict[str(t_label)],(xmin,ymin-10),cv2.FONT_HERSHEY_SIMPLEX, 0.3,true_color,1)
                
                for j,o_bbox,o_label,o_score in zip(range(boxes.shape[0]),boxes,cls_outs,obj_score): 
                    if o_score<score_threshold:continue
                    if o_score>max_score:max_score=o_score
                    
                    xcenter,ycenter,width,height = o_bbox.numpy()
                    xmin,ymin,xmax,ymax=int(xcenter-(width/2)),int(ycenter-(height/2)),int(xcenter+(width/2)),int(ycenter+(height/2)) # not scaled by image size, as this is aleardy done in the model - is it?
                    label=np.argmax(o_label.numpy())
                    out_color=(0,255,0)
                    cv2.rectangle(image_np, (xmin,ymin),(xmax,ymax),out_color,1)
                    cv2.putText(image_np,label_dict[str(label)]+f" : {str(o_score.numpy())}",(xmin,ymin-10),cv2.FONT_HERSHEY_SIMPLEX, 0.3,out_color,1)

                cv2.imwrite(str(output_directory)+f"predicted_image_{str(i)}.jpg",image_np)
                #print(f"saved image {str(i)} of {len(outputs)}")
            print(f"saved images to {str(output_directory)}")
        else:
            max_score=0
            for i,output,target,image in zip(range(len(outputs)),outputs,targets,images):  
                out_boxes=output["boxes"]
                out_labels=output["labels"]
                out_scores=output["scores"]
                target_boxes=target["boxes"]
                target_labels=target["labels"]
                if self.do_masks:
                    out_masks=output['masks']
                    if "512" in self.cfg.MODEL_NAME:
                        new_out_masks=[]
                        for mask in out_masks:
                            if torch.is_tensor(mask):
                                if len(mask.shape)!=4:
                                    mask=mask.unsqueeze(0)
                                
                                new_out_masks.append(torch.nn.functional.interpolate(mask, scale_factor=2, mode='bilinear', align_corners=True))
                            else:
                                mask=torch.Tensor(mask)
                                if len(mask.shape)!=4:
                                    mask=mask.unsqueeze(0)
                                if mask.shape[-1]==256:
                                    torch.nn.functional.interpolate(mask, scale_factor=2, mode='bilinear', align_corners=True)
                                mask=mask.numpy()[0]
                                new_out_masks.append(mask)
                        out_masks=new_out_masks
                    target_masks=target['masks']
                image_np=image
                overlayed_image=image_np.copy()
                for t_bbox ,t_label in zip(target_boxes,target_labels):
                    xmin,ymin,xmax,ymax = t_bbox.numpy().astype(int)
                    true_color=(0,0,255)
                    cv2.rectangle(image_np, (xmin,ymin),(xmax,ymax),true_color,2)
                    cv2.putText(image_np,"true_label: "+label_dict[str(t_label.numpy())],(xmin,ymin-10),cv2.FONT_HERSHEY_SIMPLEX, 0.3,true_color,1)
                #print(f"number of bounding boxes detected : {len(out_boxes)}")
                #print(f"labels detected : {out_labels}")
                #print(f"scores of detected boxes : {out_scores}")
                if not self.do_masks:
                    out_masks=range(len(out_boxes))
                for j,o_bbox,o_label,o_score,o_mask in zip(range(len(out_boxes)),out_boxes,out_labels,out_scores,out_masks):
                    if o_score<score_threshold:continue
                    if o_score>max_score:max_score=o_score
                    out_color=(0,255,0)
                    xmin,ymin,xmax,ymax = o_bbox.numpy().astype(int)
                    cv2.rectangle(image_np, (xmin,ymin),(xmax,ymax),out_color,1)
                    cv2.putText(image_np,label_dict[str(o_label.numpy())]+f" : {str(o_score.numpy())}",(xmin,ymin-10),cv2.FONT_HERSHEY_SIMPLEX, 0.3,out_color,1)
                    if self.do_masks:
                        overlay_color = (0, 255, 0)
                        #print(f" shape of omask  {o_mask.shape} for image overlay")
                        if len(o_mask.shape)==4:
                            o_mask=o_mask[0]
                        #print(f" shape of omask  {o_mask.shape} for image overlay")
                        overlayed_image[o_mask[0]>mask_threshold] = overlay_color
                        
                
                    if j>=max_predicted_boxes_per_image:break
                
                if self.do_masks:
                    out_image=cv2.addWeighted(image_np,0.5,overlayed_image,0.5,0)
                    image_np=out_image
                    
                cv2.imwrite(str(output_directory)+f"predicted_image_{str(i)}.jpg",image_np)
                #print(f"saved image {str(i)} of {len(outputs)}")
            print(f"saved {len(outputs)} images to {str(output_directory)}")

    def search_hyperparameters(
            self,
            result_dict,
            score_thresh_range=[0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95],
            iou_thresh_hit_range=[0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95],
            mask_iou_thresh_range=[0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95],
            score_to_optimize="classification_f1"):
        print(f"searching hyperparameters in validation set")
        #result_dict=self.results_dict["validation_results"]
        outputs=result_dict["outputs"]
        targets=result_dict["targets"]
        label_dict=result_dict["label_dict"]
        if self.cfg.MODEL=="yolo":
            outputs=self.yolo_nms(outputs)    
        
        raw_score_array=np.zeros((len(score_thresh_range),len(iou_thresh_hit_range)),dtype=np.float32)
        score_dict={
            "objectness_precison":raw_score_array.copy(),
            "classification_precision":raw_score_array.copy(),
            "objectness_recall":raw_score_array.copy(),
            "classification_recall":raw_score_array.copy(),
            "objectness_f1":raw_score_array.copy(),
            "classification_f1":raw_score_array.copy(),
        }
        all_confusion_matrices=[]
        for i,score_threshold in enumerate(score_thresh_range):
            confusion_matrices_row=[]
            for j,iou_thresh_hit in enumerate(iou_thresh_hit_range):
                metrics=self.calculate_metrics(outputs,targets,label_dict,score_threshold,0,iou_thresh_hit,calc_mask_iou=False)
                combined_scores=self.calculate_scores(metrics)
                score_dict["objectness_precison"][i,j]=combined_scores["objectness_precison"]
                score_dict["classification_precision"][i,j]=combined_scores["classification_precision"]
                score_dict["objectness_recall"][i,j]=combined_scores["objectness_recall"]
                score_dict["classification_recall"][i,j]=combined_scores["classification_recall"]
                score_dict["objectness_f1"][i,j]=combined_scores["objectness_f1"]
                score_dict["classification_f1"][i,j]=combined_scores["classification_f1"]
                #print(f"score calculated for hyperparameter combination {i*len(iou_thresh_hit_range)+1+j} of {len(iou_thresh_hit_range)*len(score_thresh_range)}")
                confusion_matrices_row.append(metrics["confusion_matrix"])
            all_confusion_matrices.append(confusion_matrices_row)
        best_score_thresh_idx, best_iou_thresh_idx = np.unravel_index(np.argmax(score_dict[score_to_optimize]),score_dict[score_to_optimize].shape)
        #print(f"score idx is {best_score_thresh_idx} iou score idx is {best_score_thresh_idx}")
        score_dict["confusion_matrix"]=all_confusion_matrices[best_score_thresh_idx][best_iou_thresh_idx]
        score_dict["label_dict"]=label_dict
        best_score_thresh=score_thresh_range[best_score_thresh_idx]
        best_iou_thresh=iou_thresh_hit_range[best_iou_thresh_idx]
        best_hyperparams={"best_score_thresh":best_score_thresh,"best_iou_thresh":best_iou_thresh}

        self.save_heatmap_hyperparameter_search(copy.deepcopy(score_dict),score_thresh_range,iou_thresh_hit_range)
        
        labels_score_dict=self.make_and_save_pr_curves_calc_auc(outputs,targets,label_dict,score_thresh_range,best_iou_thresh)
        if self.cfg.MODEL=="maskrcnn":
            best_mask_thresh=self.search_mask_thresh_hyperparameter(outputs,targets,mask_iou_thresh_range,best_hyperparams)
            best_hyperparams["best_mask_thresh"]=best_mask_thresh
        else:
            best_hyperparams["best_mask_thresh"]=None
        print(f"best hyperparams are {best_hyperparams}")
        score_dict["score_thresh_range"]=score_thresh_range
        score_dict["iou_thresh_range"]=iou_thresh_hit_range
        score_dict["mask_iou_thresh_range"]=mask_iou_thresh_range
        return score_dict,labels_score_dict,best_hyperparams

    def make_and_save_pr_curves_calc_auc(self,outputs,targets,label_dict,score_thresh_range,best_iou_thresh,plot_images=True):
        all_scores_per_label={}
        for score_thresh in score_thresh_range:
            metrics=self.calculate_metrics(outputs,targets,label_dict,score_thresh,0,best_iou_thresh,calc_mask_iou=False)
            scores_per_label=self.calculate_scores_per_label(metrics)

            #fill all_scores_per_label_dict with the per label scores for each threshold       
            for label_key in scores_per_label.keys():
                if not label_key in all_scores_per_label.keys():
                    all_scores_per_label[label_key]={}
                for score_key in scores_per_label[label_key].keys():
                    if not score_key in all_scores_per_label[label_key].keys():
                        all_scores_per_label[label_key][score_key]=[scores_per_label[label_key][score_key]]
                    else:
                        all_scores_per_label[label_key][score_key].append(scores_per_label[label_key][score_key])
                        
        for label_key in all_scores_per_label.keys():
            #flip order of precision and recall and add starting points and end points
            precision=all_scores_per_label[label_key]["classification_precision"][::-1]
            recall=all_scores_per_label[label_key]["classification_recall"][::-1]
            precision=[precision[0]]+precision+[0]
            recall=[0]+recall+[1]
            #calc auc-pr
            auc=self.calc_auc(precision,recall)
            #make pr curve
            if plot_images:
                self.plot_pr_curve(precision,recall,auc,label_key)
            
            #determine best f1 score and corresponding score_thresh
            cls_f1_arr=np.asarray(all_scores_per_label[label_key]["classification_f1"])
            cls_f1_best=np.max(cls_f1_arr)
            cls_f1_best_thresh=score_thresh_range[np.argmax(cls_f1_arr)]
            all_scores_per_label[label_key]["auc_pr"]=auc
            all_scores_per_label[label_key]["f1_max"]=cls_f1_best
            all_scores_per_label[label_key]["f1_max_thresh"]=cls_f1_best_thresh
        return all_scores_per_label
            
            
    def calc_auc(self,precision,recall):
        auc=0
        for i in range(1,len(recall)):
            auc+=(recall[i]-recall[i-1])*((precision[i-1]+precision[i])/2)
        return auc
            
    def plot_pr_curve(self,precision,recall,auc,label):
        if not os.path.exists(self.cfg.PLOT_DIR+f'{self.cfg.MODEL_NAME}/') or not os.path.isdir(self.cfg.PLOT_DIR+f'{self.cfg.MODEL_NAME}/'):
            os.makedirs(self.cfg.PLOT_DIR+f'{self.cfg.MODEL_NAME}/')
        # Plot the Precision-Recall curve
        plt.figure()
        plt.grid(True)
        plt.xlim(0, 1)
        plt.ylim(0, 1)
        plt.plot(recall, precision, color='darkorange', lw=2)
        plt.fill_between(recall, precision, alpha=0.5, color='darkorange')
        plt.xlabel('Recall')
        plt.ylabel('Precision')
        plt.title(f'Precision-Recall Curve for {label}')
        #print(f"saving pr_curve for {self.cfg.MODEL_NAME} {label}")
        plt.savefig(self.cfg.PLOT_DIR+f'{self.cfg.MODEL_NAME}/'+self.cfg.MODEL_NAME+f'_{label}_pr_curve')
        plt.cla() #attention

        
    #calculates the optimal threshold value for the predicted masks
    def search_mask_thresh_hyperparameter(self,outputs,targets,mask_thresh_range,best_hyperparams):
        print(f"searching for mask threshold in validation set")
        score_threshold=best_hyperparams['best_score_thresh']
        iou_thresh_hit=best_hyperparams['best_iou_thresh']
        mask_iou_for_range_list=[]
        for output,target in zip(outputs,targets):
            target_boxes,target_labels,target_masks,out_boxes,out_scores,out_labels,out_masks=self.unify_output_target(output,target)
                
            for obox,olabel,omask,oscore in zip(out_boxes,out_labels,out_masks,out_scores):
                if oscore<score_threshold:continue
                found_fitting_prediction=False
                for tbox,tlabel,tmask in zip(target_boxes,target_labels,target_masks):
                    iou=self.calc_iou(obox,tbox)
                    if iou>=iou_thresh_hit:
                        found_fitting_prediction=True
                        # A TP is when prediction and target align. for purposes of further analysis TPs are split into wrong and right calassification TPs
                        if int(olabel)==int(tlabel):
                            if "512" in self.cfg.MODEL_NAME: ## remove this later -- dirty trick-- remove this later -- or change it into feature that allows the masks to be compressd to save memory -- for this make kernel_size/scale_factor in the nn.functional a class variable of cfg and also change relevant places in train.py that can be found when searching for the start of this comment
                                omask=torch.Tensor(omask)
                                if len(omask.shape)!=4:
                                    omask=omask.unsqueeze(0)
                                #print(f"shape of omask in search mask hyperparameters {omask.shape} - shape of tmask {tmask.shape}")
                                if omask.shape[-1]==256:
                                    omask=torch.nn.functional.interpolate(omask, scale_factor=2, mode='bilinear', align_corners=True)
                                omask=omask.numpy()
                            mask_iou_for_range=self.calc_mask_iou_for_range(omask,tmask,mask_thresh_range)
                            #print(f"mask iou for {mask_iou_for_range}")
                            mask_iou_for_range_list.append(mask_iou_for_range)
        mask_iou_for_range=np.mean(mask_iou_for_range_list,axis=0)   
        best_mask_thresh_idx=np.argmax(mask_iou_for_range)   
        best_mask_thresh=mask_thresh_range[best_mask_thresh_idx]
        return best_mask_thresh
                 
    def calc_mask_iou_for_range(self,omask,tmask,mask_thresh_range):
        mask_iou_for_range=[]
        for thresh in mask_thresh_range:
            mask_iou_for_range.append(self.calc_mask_iou(omask,tmask,thresh))
        return np.asarray(mask_iou_for_range)
    
    def calc_mask_iou(self,omask,tmask,thresh):
        mask = (omask > thresh).astype(int)
        intersection = np.logical_and(mask, tmask)
        union = np.logical_or(mask, tmask)
        iou_score = np.sum(intersection) / np.sum(union)
        if math.isnan(iou_score):
            iou_score=0
        return iou_score

    
    def save_heatmap_hyperparameter_search(self,scores_dict,score_thresh_range,iou_thresh_hit_range):
        scores_dict=copy.deepcopy(scores_dict)
        label_dict=scores_dict["label_dict"]
        confusion_matrix=scores_dict["confusion_matrix"]
        del scores_dict["confusion_matrix"]
        del scores_dict["label_dict"]
        fig,axes=plt.subplots(int(len(scores_dict)/2),2,figsize=(len(score_thresh_range)*2,len(iou_thresh_hit_range)*2))
        score_thresh_range=score_thresh_range[::-1]
        iou_thresh_hit_range=[0]+iou_thresh_hit_range
        for i,key in enumerate(scores_dict):    
            sns.heatmap(scores_dict[key][::-1,:], annot=True, fmt=".2f", cmap="YlGnBu", ax=axes[i//2,i%2], cbar=True)
            #axes[i//2,i%2].imshow(scores_dict[key],cmap='autumn',)
            axes[i//2,i%2].set_ylabel("score threshold")
            axes[i//2,i%2].set_xlabel("iou threshold")
            axes[i//2,i%2].set_title(key)
            # Set custom x and y tick labels
            axes[i//2,i%2].set_xticks(np.arange(len(iou_thresh_hit_range)))
            axes[i//2,i%2].set_xticklabels(iou_thresh_hit_range)
            
            # Set y-axis tick labels based on score_thresh_range
            axes[i//2,i%2].set_yticks(np.arange(len(score_thresh_range)))
            axes[i//2,i%2].set_yticklabels(score_thresh_range)

        plt.tight_layout(pad=10)
        print(f"saving hyperparameter heatmap for {self.cfg.MODEL_NAME}")
        plt.savefig(self.cfg.PLOT_DIR+self.cfg.MODEL_NAME+'_heatmaps_hyperparameter_search')
        plt.cla()
    def calculate_scores(self,metrics):
        FN=metrics["FN"]
        FP=metrics["FP"]
        TP=metrics["TP"]
        TP_right=TP["right_cls"]
        TP_wrong_pred=TP["wrong_cls_pred"]
        TP_wrong_target=TP["wrong_cls_target"]
        
        
        TP_right_sum=sum(value for value in TP_right.values())
        TP_wrong_sum=sum(value for value in TP_wrong_pred.values())
        FP_sum=sum(value for value in FP.values())
        FN_sum=sum(value for value in FN.values())
        try:
            objectness_precison=(TP_right_sum+TP_wrong_sum)/(TP_right_sum+TP_wrong_sum+FP_sum)
        except:
            objectness_precison=0
        try:
            classification_precision=(TP_right_sum)/(TP_right_sum+TP_wrong_sum+FP_sum)
        except:
            classification_precision=0
        try:
            objectness_recall=(TP_right_sum+TP_wrong_sum)/(TP_right_sum+TP_wrong_sum+FN_sum)
        except:
            objectness_recall=0
        try:
            classification_recall=(TP_right_sum)/(TP_right_sum+FN_sum)
        except:
            classification_recall=0
        try:
            objectness_f1=(2*objectness_precison*objectness_recall)/(objectness_precison+objectness_recall)
        except:
            objectness_f1=0
        try:
            classification_f1=(2*classification_precision*classification_recall)/(classification_precision+classification_recall)
        except:
            classification_f1=0
        combined_scores={
            "objectness_precison":objectness_precison,
            "classification_precision":classification_precision,
            "objectness_recall":objectness_recall,
            "classification_recall":classification_recall,
            "objectness_f1":objectness_f1,
            "classification_f1":classification_f1,
            "avg_box_iou":metrics["avg_box_iou"]
        }
        return combined_scores
        
    def calculate_scores_per_label(self,metrics):
        FN=metrics["FN"]
        FP=metrics["FP"]
        TP=metrics["TP"]
        TP_right=TP["right_cls"]
        TP_wrong_pred=TP["wrong_cls_pred"]
        scores_per_label={}
        for label_key in FN.keys():
            try:
                objectness_precison=(TP_right[label_key]+TP_wrong_pred[label_key])/(TP_right[label_key]+TP_wrong_pred[label_key]+FP[label_key])
            except:
                objectness_precison=0
            try:
                classification_precision=(TP_right[label_key])/(TP_right[label_key]+TP_wrong_pred[label_key]+FP[label_key])
            except:
                classification_precision=0
            try:
                objectness_recall=(TP_right[label_key]+TP_wrong_pred[label_key])/(TP_right[label_key]+TP_wrong_pred[label_key]+FN[label_key])
            except:
                objectness_recall=0
            try:
                classification_recall=(TP_right[label_key])/(TP_right[label_key]+FN[label_key])
            except:
                classification_recall=0
            try:
                objectness_f1=(2*objectness_precison*objectness_recall)/(objectness_precison+objectness_recall)
            except:
                objectness_f1=0
            try:
                classification_f1=(2*classification_precision*classification_recall)/(classification_precision+classification_recall)
            except:
                classification_f1=0
            combined_scores={
                "objectness_precison":objectness_precison,
                "classification_precision":classification_precision,
                "objectness_recall":objectness_recall,
                "classification_recall":classification_recall,
                "objectness_f1":objectness_f1,
                "classification_f1":classification_f1,
                "box_iou":metrics["box_iou"][label_key]
            }
            scores_per_label[label_key]=combined_scores
        return scores_per_label
            
        
            
    def calculate_metrics(self,outputs,targets,label_dict,score_threshold,mask_threshold,iou_thresh_hit,calc_mask_iou=True):
        #print("calculating metrics")
        for i,output,target in zip(range(len(outputs)),outputs,targets): 
            if i==0:
                FNtotal,FPtotal,TPtotal,total_confusion_matrix,MASKIOUtotal,IOUtotal=self.calculate_metrics_one_image(output,target,label_dict,score_threshold,mask_threshold,iou_thresh_hit,calc_mask_iou)

            else:
                FN,FP,TP,confusion_matrix,mask_iou_dict,iou_dict=self.calculate_metrics_one_image(output,target,label_dict,score_threshold,mask_threshold,iou_thresh_hit,calc_mask_iou)

                total_confusion_matrix+=confusion_matrix
                for key in FN.keys():
                    FNtotal[key]+=FN[key]
                    FPtotal[key]+=FP[key]
                    TPtotal["wrong_cls_target"][key]+=TP["wrong_cls_target"][key]
                    TPtotal["wrong_cls_pred"][key]+=TP["wrong_cls_pred"][key]
                    TPtotal["right_cls"][key]+=TP["right_cls"][key]
                    IOUtotal[key]+=iou_dict[key]
                    MASKIOUtotal[key]+=mask_iou_dict[key]
        avg_iou=0
        total_tp=0
        for key in IOUtotal.keys():
            avg_iou+=IOUtotal[key]
            total_tp+=TPtotal["right_cls"][key]
            if TPtotal["right_cls"][key] !=0:
                IOUtotal[key]=IOUtotal[key]/TPtotal["right_cls"][key]  
            else:
                IOUtotal[key]=0
        #print(f"iou total dict at the end of calculate_metrics is {IOUtotal}")
        if total_tp!=0:
            avg_iou/=total_tp
        else:
            avg_iou=0
        #print(f"avg iou is {avg_iou}")
        #print(f"total_tp is {total_tp}")
        #if avg_iou==0:
        #    print("reason fo avg iou of 0:")
        #    print(f"score threshold is {score_threshold}")
        #    print(f"iou threshold is {iou_thresh_hit}")
        #    print(f"number of keys in IOUtotal {len(IOUtotal.keys())}")
        #    tboxes=[]
        #    oboxes=[]
        #    for t,o in zip(targets,outputs):
        #        tboxes.append(t['boxes'])
        #        oboxes.append(o['boxes'])
        #    print(f"target boxes are{tboxes}")
        #    print(f"output boxes are {oboxes}")
        metrics={"FN":FNtotal,"FP":FPtotal,"TP":TPtotal,"confusion_matrix":total_confusion_matrix,"box_iou":IOUtotal,"avg_box_iou":avg_iou,"mask_iou":None,"avg_mask_iou":None}       
        if calc_mask_iou:
            avg_mask_iou=0
            for key in MASKIOUtotal.keys():
                avg_mask_iou+=MASKIOUtotal[key]
                if TPtotal["right_cls"][key] !=0:
                    MASKIOUtotal[key]=MASKIOUtotal[key]/TPtotal["right_cls"][key]  
                else:
                    MASKIOUtotal[key]=0 
            if total_tp!=0:
                avg_mask_iou/=total_tp
            else:
                avg_mask_iou=0
                
            metrics["mask_iou"]=MASKIOUtotal
            metrics["avg_mask_iou"]=avg_mask_iou
            print(f"avg mask iou is {avg_mask_iou} - with theshold of {mask_threshold}")
        #print(f"total_tp is {total_tp}")
        return metrics
            
    def calculate_metrics_one_image(self,output,target,label_dict,score_threshold,mask_threshold,iou_thresh_hit,calc_mask_iou):
        target_boxes,target_labels,target_masks,out_boxes,out_scores,out_labels,out_masks=self.unify_output_target(output,target)
        # calculate false negatives per class - targets without any bounding box attached
        FN={}
        FP={}
        TP={"wrong_cls_pred":{},"wrong_cls_target":{},"right_cls":{}}
        iou_dict={}
        mask_iou_dict={}
        for value in label_dict.values():
            FN[value]=0
            FP[value]=0
            TP["right_cls"][value]=0
            TP["wrong_cls_pred"][value]=0
            TP["wrong_cls_target"][value]=0
            iou_dict[value]=0
            mask_iou_dict[value]=0
        confusion_matrix=np.zeros((len(FN),len(FN)))
            
        if self.cfg.MODEL != "maskrcnn":
            target_masks=range(target_boxes.shape[0])
            out_masks=range(out_boxes.shape[0])
        # this block checks if any boxes are predicted at the target location, not taking into account the predicted class - a FN is when no predicted box fits the target box
        for tbox,tlabel,tmask in zip(target_boxes,target_labels,target_masks):
            found_fitting_prediction=False
            for obox,olabel,omask,oscore in zip(out_boxes,out_labels,out_masks,out_scores):
                if oscore<score_threshold:continue
                iou=self.calc_iou(obox,tbox)
                if iou>=iou_thresh_hit:
                    found_fitting_prediction=True
                    break
            if not found_fitting_prediction:
                FN[label_dict[str(int(tlabel))]]+=1
        for obox,olabel,omask,oscore in zip(out_boxes,out_labels,out_masks,out_scores):
            if oscore<score_threshold:continue
            found_fitting_prediction=False
            for tbox,tlabel,tmask in zip(target_boxes,target_labels,target_masks):
                iou=self.calc_iou(obox,tbox)
                if iou>=iou_thresh_hit:
                    found_fitting_prediction=True
                    # A TP is when prediction and target align. for purposes of further analysis TPs are split into wrong and right calassification TPs
                    if int(olabel)==int(tlabel):
                        TP["right_cls"][label_dict[str(int(olabel))]]+=1
                        iou_dict[label_dict[str(int(olabel))]]+=iou
                        if calc_mask_iou:
                            if "512" in self.cfg.MODEL_NAME: ## remove this later -- dirty trick-- remove this later -- or change it into feature that allows the masks to be compressd to save memory -- for this make kernel_size/scale_factor in the nn.functional a class variable of cfg and also change relevant places in train.py that can be found when searching for the start of this comment
                                omask=torch.Tensor(omask)
                                if len(omask.shape)!=4:
                                    omask=omask.unsqueeze(0)
                                if omask.shape[-1]==256:
                                    omask=torch.nn.functional.interpolate(omask, scale_factor=2, mode='bilinear', align_corners=True)
                                omask=omask.numpy()
                            mask_iou=self.calc_mask_iou(omask,tmask,mask_threshold)
                            mask_iou_dict[label_dict[str(int(olabel))]]+=mask_iou
                    else:
                        TP["wrong_cls_pred"][label_dict[str(int(olabel))]]+=1
                        TP["wrong_cls_target"][label_dict[str(int(tlabel))]]+=1
                    confusion_matrix[int(olabel),int(tlabel)]+=1
                        
                    
            # a FP is when no target fits the predicted box    
            if not found_fitting_prediction:
                FP[label_dict[str(int(olabel))]]+=1
        #print(f"iou dict at the end of calculate_metrics_one_image is {iou_dict}")
        return FN,FP,TP,confusion_matrix,mask_iou_dict,iou_dict
                
            
    def unify_output_target(self,output,target,):
        target=self.unify_target(target)
        output=self.unify_output(output)
        return target["boxes"],target["labels"],target["masks"],output["boxes"],output["scores"],output["labels"],output["masks"]
            
    def unify_output(self,output):
        out_masks=None
        new_output={}
        if self.cfg.MODEL=="yolo":
            boxes=output[:,:4]
            boxes=np.asarray([self.yolo2box(box) for box in boxes])
            out_scores=output[:,4]
            cls_outs=output[:,5:]
            out_labels=np.argmax(cls_outs,axis=1)       

        else:            
            boxes=output["boxes"].numpy()
            out_labels=output["labels"].numpy()
            out_scores=output["scores"].numpy()
            if self.cfg.MODEL=="maskrcnn":
                out_masks=output['masks'].numpy()
                
        new_output["boxes"]=boxes
        new_output["labels"]=out_labels
        new_output["scores"]=out_scores
        new_output["masks"]=out_masks
        return new_output
    
    def unify_target(self,target):
        target_masks=None
        new_target={}
        if self.cfg.MODEL=="yolo":
            
            target_mask=target[:,-1].numpy().astype(bool)
            target_boxes=target[:,1:5].numpy()
            target_boxes=np.asarray([self.yolo2box(box) for box in target_boxes])
            target_labels=target[:,0].numpy()
            target_boxes=target_boxes[target_mask]
            target_labels=target_labels[target_mask]
        else:            
            target_boxes=target["boxes"].numpy()
            target_labels=target["labels"].numpy()
            if self.cfg.MODEL=="maskrcnn":
                target_masks=target['masks'].numpy()
        new_target["boxes"]=target_boxes
        new_target["labels"]=target_labels
        new_target["masks"]=target_masks
        return new_target
                
    def yolo_nms(self,outputs):
        for i in range(len(outputs)): 
            outputs[i]=self.yolo_nms_one_image(outputs[i])
            #print(f"performed nms for image {i+1} of {len(outputs)}")
        return outputs
        
    def yolo_nms_one_image(self,output): 
        sorted_indices= np.argsort(output[:,4].numpy())
        sorted_indices=sorted_indices[::-1]
        output=output[sorted_indices.copy()] #sort the output array by objectness score     
        boxes=output[:,:4].numpy()
        obj_score=output[:,4].numpy()
        obj_score_min=0.1
        suppression_mask=np.ones((obj_score.shape[0]))
        for i in range(obj_score.shape[0]):
            if i>=obj_score.shape[0]-1: continue
            if obj_score[i]<obj_score_min:
                suppression_mask[i:]=0
                break
            if suppression_mask[i]:
                obox=self.yolo2box(boxes[i])
                for j in range((i+1),obj_score.shape[0]):
                    if suppression_mask[j]:
                        if obj_score[j]<obj_score_min:
                            suppression_mask[j:]=0
                            break
                        sbox=self.yolo2box(boxes[j])
                        iou=self.calc_iou(obox,sbox)
                        if iou>self.cfg.IOU_NMS_THRESH:
                            suppression_mask[j]=0
        #print(f"output shape before nms {output.shape}")
        output=output[suppression_mask.astype(bool)]
        #print(f"output shape after nms {output.shape}")
        return output       
            
            
    def calc_iou(self,box1,box2):
        # Extract coordinates from the input boxes
        xmin1, ymin1, xmax1, ymax1 = box1
        xmin2, ymin2, xmax2, ymax2 = box2

        # Calculate the area of each bounding box
        area_box1 = (xmax1 - xmin1) * (ymax1 - ymin1)
        area_box2 = (xmax2 - xmin2) * (ymax2 - ymin2)

        # Calculate the coordinates of the intersection box
        x1_intersection = max(xmin1, xmin2)
        y1_intersection = max(ymin1, ymin2)
        x2_intersection = min(xmax1, xmax2)
        y2_intersection = min(ymax1, ymax2)

        # Check if there is a valid intersection (non-negative width and height)
        if x2_intersection > x1_intersection and y2_intersection > y1_intersection:
            # Calculate the area of the intersection box
            area_intersection = (x2_intersection - x1_intersection) * (y2_intersection - y1_intersection)

            # Calculate the IoU
            iou = area_intersection / (area_box1 + area_box2 - area_intersection)
        else:
            # No intersection, IoU is 0
            iou = 0.0

        return iou            
        
    def yolo2box(self,bbox):
        xcenter,ycenter,width,height = bbox
        xmin,ymin,xmax,ymax=int(xcenter-(width/2)),int(ycenter-(height/2)),int(xcenter+(width/2)),int(ycenter+(height/2))
        return xmin,ymin,xmax,ymax
    
    def apply_score_thresh(self,outputs,score_thresh):
        for output in outputs:
            if type(output['scores'])==np.ndarray:
                output['scores']=torch.from_numpy(output['scores'])
            print(f"type of scores in output is {type(output['scores'])}")
            print(f"output scores shape {output['scores'].shape} - max {torch.max(output['scores'])} - min {torch.min(output['scores'])}")
            score_mask=(output['scores'] > score_thresh)
            output["scores"]=output["scores"][score_mask]
            output["labels"]=output["labels"][score_mask]
            output["boxes"]=output["boxes"][score_mask]
            if self.do_masks:output["masks"]=output["masks"][score_mask]
            order=torch.argsort(output["scores"], descending=True)

            output["scores"]=output["scores"][order]
            output["labels"]=output["labels"][order]
            output["boxes"]=output["boxes"][order]
            if self.do_masks:output["masks"]=output["masks"][order]
        return outputs
            
                
        
class LightOrganizer(Organizer):
    def __init__(self,cfg,create_model=True):
        self.cfg=cfg
        self.create_model=create_model
        if cfg.MODEL=="maskrcnn":
            self.do_masks=True
            self.yolo=False
        elif cfg.MODEL=="yolo":
            self.do_masks=False
            self.yolo=True
        else:
            self.yolo=False
            self.do_masks=False
        if create_model:
            print("hello")
            if cfg.LOAD_MODEL_PATH:
                self.model,self.params,optimizer_state_dict=load_model(cfg)            
                self.optimizer,self.optimizer_descriptor=get_optimizer(cfg,self.params)  ## loading of the optimizer state currently disabled
                #print(optimizer_state_dict)
                #self.optimizer.load_state_dict(optimizer_state_dict)
            else:
                self.model,self.params=get_model_and_params(cfg)
                
                self.optimizer,self.optimizer_descriptor=get_optimizer(cfg,self.params)
                
            if cfg.DATASET in ["alldescriptons","persondescriptions"]:
                self.tokenizer=DescriptionTokenizer(BertTokenizer.from_pretrained('bert-base-uncased'),cfg.DEVICE)
                
    def infer_image_list(self,image_list:list,description_list=None,batch_size=0,score_thresh=0):
        outputs=[]
        if not self.create_model:
            raise Exception("cannot infer images when create_model is False")
        if self.cfg.DATASET in ["alldescriptons","persondescriptions"] and type(description_list)==type(None):
            print("inferring without using descriptions")
            description_list=[]
            for i in range(len(image_list)):
                description_list.append("")
        if self.cfg.DATASET in ["alldescriptons","persondescriptions"] and len(description_list)!=len(image_list):
            raise Exception("image_list and description_list are not of the same length")
        if batch_size!=0:
            n_batches=(len(image_list)//batch_size)+1
            for i in range(n_batches):
                print(f"inferring batch {i} of {max(n_batches,1)}")
                current_image_list=image_list[i:min(len(image_list),(i+1)*batch_size)]
                for i,current_image in enumerate(current_image_list):
                    processed_image,h,w=preprocess_image(current_image,target_height=self.cfg.RESIZE_TO,target_width=self.cfg.RESIZE_TO)
                    processed_image/=255.0
                    current_image_list[i]=torch.from_numpy(processed_image).permute(2,0,1).to(self.cfg.DEVICE)
                images=torch.stack(current_image_list,dim=0).to(self.cfg.DEVICE)
                    
                if self.cfg.DATASET in ["alldescriptons","persondescriptions"]:
                    current_description_list=description_list[i:min(len(image_list),(i+1)*batch_size)]                            
                    descriptions, attention_mask= self.tokenizer.tokenize(current_description_list)
                    inputs={'images':images,'descriptions':descriptions,'attention_mask':attention_mask}
                else:
                    inputs=images
                self.model.eval()
                with torch.no_grad():
                    output=self.model(inputs)
                self.model.train()
                if self.cfg.MODEL=="yolo":
                    output=self.yolo_nms(output)
                output=self.process_outputs(output)
                output=self.apply_score_thresh(output,score_thresh)
                outputs+=output
        else:
            for i,image in enumerate(image_list):
                image,h,w=preprocess_image(image,target_height=self.cfg.RESIZE_TO,target_width=self.cfg.RESIZE_TO)
                image/=255.0
                image=torch.from_numpy(image).permute(2,0,1).to(self.cfg.DEVICE)
                print(f"inferring image {i} of {len(image_list)}")
                images=torch.stack([image],dim=0).to(self.cfg.DEVICE)
                if self.cfg.DATASET in ["alldescriptons","persondescriptions"]:                            
                    descriptions, attention_mask= self.tokenizer.tokenize([description_list[i]])
                    inputs={'images':images,'descriptions':descriptions,'attention_mask':attention_mask}
                else:
                    inputs=images
                self.model.eval()
                with torch.no_grad():
                    output=self.model(inputs)
                self.model.train()
                if self.cfg.MODEL=="yolo":
                    output=self.yolo_nms(output)
                output=self.process_outputs(output)
                output=self.apply_score_thresh(output,score_thresh)
                outputs+=output
        return outputs
    
    