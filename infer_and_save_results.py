from organizer import Organizer
import pickle
import torch
model_names=[
                #alldataset results
             #"maskrcnn_alldataset_imsize512",
             #"maskrcnn_alldataset_imsize512_conv2dhead",
             #"maskrcnn_alldataset_imsize256",
             #"maskrcnn_alldataset_imsize256_conv2dhead",
             "fasterrcnn_alldataset_imsize512",
             #"fasterrcnn_alldataset_imsize512_conv2dhead",
             #"fasterrcnn_alldataset_imsize256",
             #"fasterrcnn_alldataset_imsize256_conv2dhead",
             #"yolodark_alldataset_imsize512",
             #"yolodark_alldataset_imsize256",
             #"yoloresnet_alldataset_imsize512",
             #"yoloresnet_alldataset_imsize256",
                #persondataset results
             "fasterrcnn_persondataset_imsize512",
             #"fasterrcnn_persondataset_imsize512_conv2dhead",
             #"fasterrcnn_persondescriptiondataset_imsize512",
             #"fasterrcnn_persondescriptiondataset_imsize512_conv2dhead",
             #"maskrcnn_persondataset_imsize512",
             #"maskrcnn_persondataset_imsize512_conv2dhead",
             #"maskrcnn_persondescriptiondataset_imsize512",
             #"maskrcnn_persondescriptiondataset_imsize512_conv2dhead",
             #"yolodark_persondataset_imsize512",
             #"yoloresnet_persondataset_imsize512",
             ]

for model_name in model_names:
    print(f"calculating performance and generating images for {model_name}")
    path_to_results_pickled_dict=f"/scratch/users/jpeters4/Annotationen/person_detection_outputs/results/{model_name}.pkl"

    with open(path_to_results_pickled_dict,'rb') as file:
        loaded_dict=pickle.load(file)
    cfg=loaded_dict["cfg"]
    if not ("conv2d" in model_name):
        cfg.__class__.USE_CONV_BOX_HEAD =False    
    else:
        cfg.__class__.USE_CONV_BOX_HEAD =True   
    cfg.NUM_WORKERS=0
    cfg.LOAD_BEST_MODEL=False
    cfg.LOAD_LAST_MODEL=True
    cfg.DESCRIPTION_DROPOUT=0.0 # determines the fraction of descriptions to be used for inference. 0.0 means all descriptions, while 1.0 means none
    cfg.SHRINK_DATASET_FRACTION=1
    cfg.BATCH_SIZE=24
    cfg.CSV_DIR="/scratch/users/jpeters4/csv/"
    org=Organizer(cfg)
    cfg.MODEL_NAME=cfg.MODEL_NAME+"_lastmodel"
    #cfg.MODEL_NAME=model_name+"_descriptions"
    print(f"MODEL_NAME is {cfg.MODEL_NAME}")
    org.set_results_dict(loaded_dict)
    org.calc_performance_and_generate_images()
    print(f"finished inference and saving results for {model_name}\n\n\n")
    res_dict=org.results_dict
    del res_dict["validation_results"]
    del res_dict["test_results"]
    del res_dict["validation_scores"]
    del res_dict["train_loss_accumulator"]
    del res_dict["validation_loss_accumulator"]
    for key in res_dict.keys():
        print(f"{key}\t\t\t{res_dict[key]}\n\n\n")
print(f"inference finished for {model_names}")