from organizer import LightOrganizer
import pickle
import cv2
import numpy as np
model_names=[
             #alldataset results
             #"maskrcnn_alldataset_imsize512",
             #"maskrcnn_alldataset_imsize256",
             #"fasterrcnn_alldataset_imsize512",
             #"fasterrcnn_alldataset_imsize256",
             #"yolodark_alldataset_imsize512",
             #"yolodark_alldataset_imsize256",
             #"yoloresnet_alldataset_imsize512",
             #"yoloresnet_alldataset_imsize256",
                #persondataset results
             #"fasterrcnn_persondataset_imsize512",
             "maskrcnn_persondataset_imsize512",
             #"fasterrcnn_persondescriptiondataset_imsize512",
             #"maskrcnn_persondescriptiondataset_imsize512",
             #"yolodark_persondataset_imsize512",
             #"yoloresnet_persondataset_imsize512",
             ]

for model_name in model_names:
    print(f"calculating performance and generating images for {model_name}")
    path_to_results_pickled_dict=f"/scratch/users/jpeters4/Annotationen/person_detection_outputs/results/{model_name}.pkl"

    with open(path_to_results_pickled_dict,'rb') as file:
        loaded_dict=pickle.load(file)
    
    cfg=loaded_dict["cfg"]
    label_dict=loaded_dict['test_results']["label_dict"] # the labels returned from inference,cast into strings are the keys to the label dict.
    best_hyperparameters=loaded_dict['best_hyperparameters']
    score_thresh=best_hyperparameters["best_score_thresh"]
    print(f"best hyperparameters: {best_hyperparameters}")

    cfg.LOAD_BEST_MODEL=True

    org=LightOrganizer(cfg)
    test_images_names=['217920B-Curti(2001)Taf13b_cut_PTRMeleager_MM_16042021.jpg', '217921A-KatComachio(1989)67Nr25_cut_PTRMeleager_MM_06022021.jpg', '217933A-AN00320922_001_cut_PTRMeleager_MM_24032021.jpg', '217936A-CVAD86Taf49,1_cut_PTRMeleager_MM_20022021.jpg', '217936B-CVAD86Taf48,2_cut_PTRMeleager_MM_20022021.jpg', '217940A-AN00542780_001_cut_PTRMeleager_MM_01042021.jpg', '217940B-542776001_cut_PTRMeleager_MM_30032021.jpg', '217941A-DP114757_cut_PTRMeleager_MM_22032021.jpg', '217941B-DP114758_cut_PTRMeleager_MM_21032021.jpg', '217944A-Ghali-Kahil(1955)Taf24,2_cut_PTRMeleager_MM_09042021.jpg']

    test_images_list=[]
    for imname in test_images_names:
        test_images_list.append(cv2.imread(cfg.IMAGE_DIR+imname).astype(np.float32))
    #print(test_images_list)
    outputs=org.infer_image_list(test_images_list,description_list=None,score_thresh=score_thresh)#batch_size=3)
    print(outputs)

