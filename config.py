import torch
import sys
sys.path.append('..')
import os


class ImageSizeNotDivisibleBy32Exception(Exception):
    pass

class YoloCantUseDescriptionsException(Exception):
    pass


class Cfg():
    def __init__(self):
        pass
    # determine which model to use, can be "yolo" "fasterrcnn" or "maskrcnn"
    MODEL="yolo"
    DATASET="all"
    MODEL_NAME=None
    LOAD_BEST_MODEL=False
    LOAD_LAST_MODEL=False
    LOAD_MODEL_PATH=None # you can specify the model to load here, it should not be necessary though, as the model path will be constructed from the model name
    BATCH_SIZE = 8# increase / decrease according to GPU memeory
    
    # Size for input images to be resized to. for pretrained models needs to be the same as the model was trained on. 
    # ALWAYS HAS TO BE A MULTIPLE OF 32 - also impacts memory use
    RESIZE_TO = 256 # resize the image for training and transforms   
    
    #Determine Anchor sizes and aspect ratios, only necessary for rcnn models
    # anchor_size_divideby needs to be of length 5 !! -  tested for [16,8,4,2,1] 
    anchor_size_divideby=[16,8,4,2,1] 
    ASPECT_RATIOS=((0.5, 1.0, 2.0),)
        
    # How long to train and finetune the model
    NUM_EPOCHS_TRAIN=40
    NUM_EPOCHS_FINETUNE=0
    
    # Optimizer configuration
    OPTIMIZER_STRING="Adam" # must be "SGD" "Adam" or "Adagrad"
    # not every hyperparameter is relevant for every optimizer - most important is learning rate LR. see which hyperparameters are relevant for which optimizer in the _get_optimizer function in ObjectDetectionTrainer in train.py
    LR=0.0001
    FINETUNE_LR=0.0001
    LR_DECAY=0.0
    MOMENTUM=0.5
    WEIGHT_DECAY=0.0
    BETAS=(0.9,0.999)
    EPS=1e-10
    INITIAL_ACCUMULATOR_VALUE=0
    AMSGRAD=False
    
    # For Training Models with description, during training it may be of use to use dropout on the descriptions to make sure the model is robust and not dependent on them
    DESCRIPTION_DROPOUT=0.5
    
    # ROI_POOLING_OUTPUT_SIZE governs the resolution of the parts of the image the region proposal network computes objectness on - standard value 16
    ROI_POOLING_OUTPUT_SIZE=16
    
    # hyperparameters for classification
    INTERSECTION_OVER_UNION_THRESHOLD=0.5
    DETECTION_SCORE_THRESHOLD=0.5
    
    # Number of workers for dataloader
    NUM_WORKERS=4
    
    #these values come from the fasterrcnn implementation and are mean and std of the data the backbone was trained on - these are not currently used as MyGeneralizedRCNNTransform skips normalization
    DATASET_MEAN=[0.485, 0.456, 0.406]
    DATASET_STD=[0.229, 0.224, 0.225]    

    TRAIN_VAL_TEST_SPLIT=[0.75,0.15,0.1]

    # for using only a portio of the dataset, may be useful for testing. value of 1 leaves dataset as is
    SHRINK_DATASET_FRACTION=1
    LOAD_DATASET=False
    # CAUTION - WORK IN PROGRESS - Mutli gpu computation may not work as intended
    USE_MULTI_GPU=False
    
    # location to save model and plots
    OUT_DIR = None
    IMAGE_DIR=None
    JSON_DIR=None
    MODELS_DIR=None
    RESULTS_DIR=None
    PLOT_DIR=None
    PREDICTED_IMAGES_DIR=None
    DATASET_DIR=None
    CSV_DIR=None
    # relevant for all and alldescription datasets. if set to false will use all 300 labels
    USE_ONLY_SPECIFIC_LABELS=True
    #this one only includes humans and important objects
    SPECIFIC_LABELS=['head','arm','leg','hand','foot','horn','wing','sword','pickaxe','spear','axe','club','pelta','shield','helmet','thunderbolt','drinking_horn','trumpet','ball','phallos','mask','amphora','sceptre','cart','basket','chisel','whip','tablet','knife','instrument','curass','flute']
        
    # this one includes all the animals for a total of 54 classes
    #SPECIFIC_LABELS=['head','arm','leg','hand','foot','horn','wing','sword','pickaxe','spear','axe','club','pelta','thunderbolt','drinking_horn','trumpet','ball','phallos','mask','amphora','sceptre','cart','basket','chisel','whip','tablet','knife','instrument','curass','flute','horse','dog','bird','goose','owl','deer','animal','hen','cock','hare','dolphin','scorpion','snake','griffin','calf','fish','ram','bull','eagle','hydra','crane','cow','octopus','mule']
    
    LABELS_CHILDREN_OF_PERSON=['arm','leg','horn']# removed 'head' , 'wing'
    #SPECIFIC_LABELS=['head','hand']
    
    # relevant to person and persondescription dataset, can be extended to creatures if needed
    PERSON_LABELS=['man','youth','woman','girl','boy','god','goddess','child']
        
    # should not be manually set. is set by the trainer after calculating the number of classes in the dataset
    NUM_CLASSES=None
    # relevant to the fasterrcnn implementation, determines intersection over union thresholds for determining if detected objects are foreground or background in the region proposal network
    #RPN variables standerd values are fg=0.7 and bg=0.3
    RPN_FG_IOU_THRESH=0.7
    RPN_BG_IOU_THRESH=0.3
    # the same in the region of interest head responsible for classification
    #ROI variable standard values are 0.5 and 0.5 -- best results for 0.4 0.35
    BOX_FG_IOU_THRESH=0.5
    BOX_BG_IOU_THRESH=0.5    
    #IOU threshold for non maximum suppression
    IOU_NMS_THRESH=0.7
    # set the maximum number of objects present in the ground thruth to be passed to yolo - this is necessary because yolo expects the same number of objects every time, while some may be empty
    YOLO_OBJECT_MAX=100
    YOLO_USE_RESNET_BACKBONE=True
    # wether to use GeneralizedRCNNTransform - only useful if you want to detect a range of different image sizes
    USE_GENERALIZEDRCNNTRANSFORM=False
    # whether to use the more resource demanding convolutional box head
    USE_CONV_BOX_HEAD=True
    # images are converted to grayscale prior to being passed to the model. the model still expects three channels. without the three channel trick each of these channels is equal and contains the grayscale pixel value.
    # with the three channel trick two of the channels are used to carry information from a higher resolution. see preprocess image function in DefaultObjectDataset in datasets.py
    USE_THREE_CHANNEL_TRICK=True
    
    # flag to specify if inference should be run while training to calculate a running value for accuracy on train and validation datasets. Doubles the time the model needs to run. - currently not working properly
    EVAL_WHILE_TRAINING=False
    
    # CUDA device, a change here shouldnt be necessary
    DEVICE = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    
    def check_cfg(self):
        
        desc_datasets=["alldescriptons","persondescriptions"]
        
        if self.RESIZE_TO % 32 != 0:
            raise ImageSizeNotDivisibleBy32Exception
        if self.DATASET in desc_datasets and self.MODEL=="yolo":
            raise YoloCantUseDescriptionsException
        
        self.TRAIN_SPLIT={'mode':'train',
            'split':self.TRAIN_VAL_TEST_SPLIT}
        # validation images split dict
        self.VAL_SPLIT={'mode':'val',
            'split':self.TRAIN_VAL_TEST_SPLIT}
        # test split
        self.TEST_SPLIT={'mode':'test',
            'split':self.TRAIN_VAL_TEST_SPLIT}
        
        self.make_filesystem()
        
        
        if self.LOAD_BEST_MODEL and self.LOAD_LAST_MODEL:
            raise Exception("cfg.LOAD_BEST_MODEL and cfg.LOAD_LAST_MODEL can not be True at the same time")
        if self.LOAD_BEST_MODEL:
            self.LOAD_MODEL_PATH=self.MODELS_DIR+self.MODEL_NAME+'_best_model.pth'
        if self.LOAD_LAST_MODEL:
            self.LOAD_MODEL_PATH=self.MODELS_DIR+self.MODEL_NAME+'_last_model.pth'
        
        #self.ANCHOR_SIZES=((int(RESIZE_TO/self.anchor_size_divideby[0]),), (int(RESIZE_TO/self.anchor_size_divideby[1]),), (int(RESIZE_TO/self.anchor_size_divideby[2]),), (int(RESIZE_TO/self.anchor_size_divideby[3]),), (RESIZE_TO/self.anchor_size_divideby[4],), )
        self.calc_anchor_sizes()
        
    def calc_anchor_sizes(self):
        anchor_sizes=()
        for i in range(len(self.anchor_size_divideby)):
            anchor_sizes=anchor_sizes+((int(self.RESIZE_TO/self.anchor_size_divideby[i]),),)
        self.ANCHOR_SIZES=anchor_sizes
        print(self.ANCHOR_SIZES)
            
    def make_filesystem(self):
        if not self.IMAGE_DIR:
            raise Exception("you need to specify IMAGE_DIR for training images")
        if not os.path.exists(self.IMAGE_DIR) or not os.path.isdir(self.IMAGE_DIR):
            raise Exception(f"The provided folder for images {self.IMAGE_DIR} does not exist")
        
        if not self.JSON_DIR:
            raise Exception("you need to specify JSON_DIR for training targets")
        if not os.path.exists(self.JSON_DIR) or not os.path.isdir(self.JSON_DIR):
            raise Exception(f"The provided folder for jsons {self.JSON_DIR} does not exist")
        
        if not self.OUT_DIR:
            raise Exception("you need to specify OUT_DIR for output location of models, results, predicted images and datasets")
        if not os.path.exists(self.OUT_DIR) or not os.path.isdir(self.OUT_DIR):
            os.makedirs(self.OUT_DIR)
            print(f"The Provided Folder for Outputs did not exist. It has been created at {self.OUT_DIR}")
            
        if self.DATASET in ["alldescriptons","persondescriptions"]:
            if not self.CSV_DIR:
                raise Exception("you need to specify CSV_DIR for training images when using a description dataset")
            if not os.path.exists(self.CSV_DIR) or not os.path.isdir(self.CSV_DIR):
                raise Exception(f"The provided folder for csv files containing descriptions {self.CSV_DIR} does not exist")
            
        if self.MODELS_DIR:
            if not os.path.exists(self.MODELS_DIR) or not os.path.isdir(self.MODELS_DIR):
                os.makedirs(self.MODELS_DIR)
                print(f"The Provided Folder for Models did not exist. It has been created at {self.MODELS_DIR}")
        else:
            self.MODELS_DIR=self.OUT_DIR+"models/"
            if not os.path.exists(self.MODELS_DIR) or not os.path.isdir(self.MODELS_DIR):
                os.makedirs(self.MODELS_DIR)
                print(f"for MODELS_DIR created folder {self.MODELS_DIR}")
                
        if self.RESULTS_DIR:
            if not os.path.exists(self.RESULTS_DIR) or not os.path.isdir(self.RESULTS_DIR):
                os.makedirs(self.RESULTS_DIR)
                print(f"The Provided Folder for Results did not exist. It has been created at {self.RESULTS_DIR}")
        else:
            self.RESULTS_DIR=self.OUT_DIR+"results/"
            if not os.path.exists(self.RESULTS_DIR) or not os.path.isdir(self.RESULTS_DIR):
                os.makedirs(self.RESULTS_DIR)
                print(f"for RESULTS_DIR created folder {self.RESULTS_DIR}")
                
        if self.PREDICTED_IMAGES_DIR:
            if not os.path.exists(self.PREDICTED_IMAGES_DIR) or not os.path.isdir(self.PREDICTED_IMAGES_DIR):
                os.makedirs(self.PREDICTED_IMAGES_DIR)
                print(f"The Provided Folder for predicted images did not exist. It has been created at {self.PREDICTED_IMAGES_DIR}")
        else:
            self.PREDICTED_IMAGES_DIR=self.OUT_DIR+"predicted_images/"
            if not os.path.exists(self.PREDICTED_IMAGES_DIR) or not os.path.isdir(self.PREDICTED_IMAGES_DIR):
                os.makedirs(self.PREDICTED_IMAGES_DIR)
                print(f"for PREDICTED_IMAGES_DIRcreated folder {self.PREDICTED_IMAGES_DIR}")
                            
        if self.PLOT_DIR:
            if not os.path.exists(self.PLOT_DIR) or not os.path.isdir(self.PLOT_DIR):
                os.makedirs(self.PLOT_DIR)
                print(f"The Provided Folder for predicted images did not exist. It has been created at {self.PLOT_DIR}")
        else:
            self.PLOT_DIR=self.OUT_DIR+"plots/"
            if not os.path.exists(self.PLOT_DIR) or not os.path.isdir(self.PLOT_DIR):
                os.makedirs(self.PLOT_DIR)
                print(f"for PLOT_DIR created folder {self.PLOT_DIR}")
                
        if self.DATASET_DIR:
            if not os.path.exists(self.DATASET_DIR) or not os.path.isdir(self.DATASET_DIR):
                os.makedirs(self.DATASET_DIR)
                print(f"The Provided Folder for datasets did not exist. It has been created at {self.DATASET_DIR}")
        else:
            self.DATASET_DIR=self.OUT_DIR+"datasets/"
            if not os.path.exists(self.DATASET_DIR) or not os.path.isdir(self.DATASET_DIR):
                os.makedirs(self.DATASET_DIR)
                print(f"for DATASET_DIR created folder {self.DATASET_DIR}")
            
        
            