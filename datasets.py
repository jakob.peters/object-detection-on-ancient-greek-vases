import torch
import cv2
import numpy as np
from matplotlib.path import Path
import os
import glob as glob
from xml.etree import ElementTree as et
import shelve
import random

from torch.utils.data import Dataset, DataLoader
from custom_utils import collate_fn, get_train_transform, get_valid_transform, get_test_transform
import sys
sys.path.append('..')
import json
import math
import pandas as pd
import random
import xmltodict
from image_utils import preprocess_image
# the dataset class
class NotImplementedException(Exception):
    pass

class DefaultObjectDataset(Dataset):
    def __init__(self, split_dir, cfg,  transforms=None, load_dataset=False, dataset_name="defaultobject"):
        print("Default dataset constructor")
        self.transforms = transforms
        self.split_dir = split_dir
        self.cfg=cfg
        self.height = cfg.RESIZE_TO
        self.width = cfg.RESIZE_TO
        self.image_dir=cfg.IMAGE_DIR
        self.json_dir=cfg.JSON_DIR
        if cfg.MODEL=="maskrcnn":
            self.do_masks=True
            self.yolo=False
        elif cfg.MODEL=="yolo":
            self.do_masks=False
            self.yolo=True
        else:
            self.yolo=False
            self.do_masks=False
        if load_dataset:
            print("loading dataset")
            print(f'loading {split_dir["mode"]} dataset')
            with shelve.open(cfg.DATASET_DIR+dataset_name) as shelf:
                ks=shelf.keys()
                for k in ks: print(k)
                self.all_images=shelf['images']
                self.all_jsons=shelf['jsons']
                self.label_dict=self.prepare_label_dict()
                self.classes=list(self.label_dict.keys()) 
        else:
            print("creating dataset")
            print(f'creating {split_dir["mode"]} dataset')
            # get all the image paths in sorted order
            self.image_paths = glob.glob(f"{self.image_dir}/*")        
            self.all_images = [image_path.split(os.path.sep)[-1] for image_path in self.image_paths]
            self.all_images = sorted(self.all_images)
            self.all_jsons = [".".join(image.split('.')[:-1])+'.json' for image in self.all_images]
            self.label_dict=self.prepare_label_dict()
            self.classes=list(self.label_dict.keys())        
            
            #remove all entries that do not contain a valid bounding box, a record of ignored images is saved in the home directory
            self.remove_images_without_boxes()
            with shelve.open(cfg.DATASET_DIR+dataset_name) as shelf:
                shelf['images']=self.all_images
                shelf['jsons']=self.all_jsons
        self.reverse_label_dict={str(v): k for k, v in self.label_dict.items()}
        # apply train val split
        n_elements=len(self.all_images)
        if(self.split_dir['mode']=='train'):
            train_cutoff=int(n_elements*self.split_dir['split'][0])
            self.all_images=self.all_images[:train_cutoff]
            self.all_jsons = self.all_jsons[:train_cutoff]
        elif(self.split_dir['mode']=='val'):
            train_cutoff=int(n_elements*self.split_dir['split'][0])
            val_cutoff=int(n_elements*(self.split_dir['split'][0]+self.split_dir['split'][1]))
            self.all_images=self.all_images[train_cutoff:val_cutoff]    
            self.all_jsons = self.all_jsons[train_cutoff:val_cutoff]    
        elif(self.split_dir['mode']=='test'):
            val_cutoff=int(n_elements*(self.split_dir['split'][0]+self.split_dir['split'][1]))
            self.all_images=self.all_images[val_cutoff:]
            self.all_jsons = self.all_jsons[val_cutoff:]           

        print("Dataset generated")
        
    def get_size(self):
        return len(self.all_images)
    def get_classes(self):
        return self.classes    
    def prepare_label_dict(self):
        pass    
    
    def remove_images_without_boxes(self):
        ids_to_remove=[]
        for idx in range(len(self.all_images)):
            # capture the corresponding json file for getting the annotations
            annot_filename = self.all_jsons[idx]
            annot_file_path = os.path.join(self.json_dir, annot_filename)
            with open (annot_file_path,'r') as f:
                annot=json.load(f)  
            box_count=0      
            for shape in annot['shapes']:
                label=shape['label']
                if self.cfg.USE_ONLY_SPECIFIC_LABELS and not label in self.cfg.SPECIFIC_LABELS:
                    continue
                box_count+=1

            if box_count==0:
                ids_to_remove.append(idx)
        ids_to_remove.sort(reverse=True)
        print(f"removed indices {ids_to_remove}")
        for idx in ids_to_remove:
            del self.all_images[idx]
            del self.all_jsons[idx]
            
    def getname(self,idx):    
        return self.all_images[idx]
    
    def shrink_dataset(self,fraction=1):
        if fraction>1:
            print(f"fraction for shrinking of dataset needs to be no larger than 1, but has been given as {fraction}. dataset remains unchanged.")
            return
        if fraction<=0:
            print(f"fraction for shrinking of dataset needs to be larger than 0 but has been given as {fraction}. dataset remains unchanged.")
            return
        n_images=len(self.all_images)
        self.all_images=self.all_images[:int(n_images*fraction)]
        

            
    def __getitem__(self, idx):
        # capture the image name and the full image path
        image_name = self.all_images[idx]
        image_path = os.path.join(self.image_dir, image_name)
        
        # capture the corresponding json file for getting the annotations
        annot_filename = self.all_jsons[idx]
        annot_file_path = os.path.join(self.json_dir, annot_filename)
        with open (annot_file_path,'r') as f:
            annot=json.load(f)
            
        # read and resize the image
        image = cv2.imread(image_path).astype(np.float32)
        #image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image_resized,original_height,original_width = preprocess_image(image,target_height=self.cfg.RESIZE_TO,target_width=self.cfg.RESIZE_TO)  #pad and resize image keeping ratio and applying three channel trick
        image_original=image_resized.copy()
        image_resized /= 255.0
        #raise Exception(f"image shape is {image_resized.shape} - with dtype {image_resized.dtype}\n\n{image_resized}")
        #image = np.transpose(image_resized, (2, 0, 1)).astype(np.float32)
        
        boxes,labels,masks=self.extract_boxes_and_labels(image,original_height,original_width,annot,self.do_masks)
        labels=[int(self.label_dict[label]) for label in labels] # convert string labels as found in the jsons to int labels needed in the targets
        # bounding box to tensor
        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        #if self.do_masks:
            #masks = torch.as_tensor(np.array(masks), dtype=torch.int8)
        # area of the bounding boxes
        try:
            area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        except:
            print("these boxes failed")
            print(boxes)
            print("with these labels")
            print(labels)
            raise BaseException
        # no crowd instances
        iscrowd = torch.zeros((boxes.shape[0],), dtype=torch.int64)
        # labels to tensor
        labels = torch.as_tensor(labels, dtype=torch.int64)
        # prepare the final `target` dictionary
        target = {}
        target["boxes"] = boxes
        target["labels"] = labels
        target["area"] = area
        target["iscrowd"] = iscrowd
        image_id = torch.tensor([idx])
        target["image_id"] = image_id
        if self.do_masks:
            target["masks"] = masks
        #print(f"labels in dataset are : {labels}")
        # apply the image transforms
        if self.do_masks:
            if self.transforms:
                sample = self.transforms(image = image_resized,
                                        bboxes = target['boxes'],
                                        labels = labels,
                                        masks = masks)
                image_resized = sample['image']
                target['boxes'] = torch.Tensor(sample['bboxes'])
                target['masks'] = torch.stack(sample['masks'])
        else:
            if self.transforms:
                sample = self.transforms(image = image_resized,
                                        bboxes = target['boxes'],
                                        labels = labels)
                image_resized = sample['image']
                target['boxes'] = torch.Tensor(sample['bboxes'])
                
        if self.yolo:
            target=self.yolo_target_transform(target)
            
        return image_resized, target, image_original, self.reverse_label_dict

    def yolo_target_transform(self,targets,grid_size=13):

        yolo_targets=torch.zeros((self.cfg.YOLO_OBJECT_MAX,6))
        
        boxes=targets['boxes']
        labels=targets['labels']
        
        for idx,(box,label) in enumerate(zip(boxes,labels)):
            if idx>=self.cfg.YOLO_OBJECT_MAX:
                print(f"more than {idx} objects in ground truth, but the YOLO_OBJECT_MAX determined in cfg is {self.cfg.YOLO_OBJECT_MAX}. only the first {idx} targets are used - total number of targets is {len(boxes)} ")
                break
            xmin, ymin, xmax, ymax = box
            object_width=(xmax-xmin)
            object_height=(ymax-ymin)
            xcenter=(xmax+xmin)/2
            ycenter=(ymax+ymin)/2            
            yolo_targets[idx]=torch.tensor([label,xcenter,ycenter,object_width,object_height,1.0])
            
        return yolo_targets
               
    def recalculate_shape_pixels(self,points,original_height,original_width):
        
        x_col=(points[:,0]/max(original_width,original_height))*self.height
        y_col=(points[:,1]/max(original_width,original_height))*self.height
        x_col=np.clip(x_col,0,self.width)
        y_col=np.clip(y_col,0,self.height)
        points[:,0]=x_col
        points[:,1]=y_col
        return points
        
  
    def extract_boxes_and_labels(self,image,original_height,original_width,annot,do_masks):
        pass
    
    def get_mask(self,points):        
        xx,yy = np.meshgrid(np.arange(0,self.width),np.arange(0,self.height))
        grid_points = np.vstack((xx.flatten(),yy.flatten())).T
        polygon_path=Path(points)
        inside_polygon = polygon_path.contains_points(grid_points)
        binary_map=inside_polygon.reshape(self.height,self.width).astype(int)
        return binary_map
    
    def __len__(self):
        return len(self.all_images)
        

    
class PersonObjectDataset(DefaultObjectDataset):
    def __init__(self, split_dir, cfg, transforms=None, load_dataset=False, dataset_name="personobject"):
        super().__init__(split_dir, cfg, transforms=transforms, load_dataset=load_dataset, dataset_name=dataset_name) 
    def remove_images_without_boxes(self):
        #remove all entries that do not contain a valid bounding box, a record of ignored images is saved in the home directory
        idx_to_remove=[]
        for idx in range(self.get_size()):
            annot_filename = self.all_jsons[idx]
            annot_file_path = os.path.join(self.json_dir, annot_filename)
            with open (annot_file_path,'r') as f:
                annot=json.load(f)
            found_person=False
            for shape in annot['shapes']:
                if shape['label'] in self.cfg.PERSON_LABELS: # removed the person attribute from check for data cleaning, instead adding from list of labels in config
                    found_person=True
            if not found_person:
                idx_to_remove.append(idx)
        
        ignored_images=''
        for i in sorted(idx_to_remove,reverse=True):
            ignored_images+=self.all_images[i]+"\n"
            self.all_images.pop(i)
            self.all_jsons.pop(i)
    
    def prepare_label_dict(self):
        label_dict={
             '__background__':0,
        }  
        label_counter=0
        for annot_filename in self.all_jsons:
            annot_file_path = os.path.join(self.json_dir, annot_filename)
            with open(annot_file_path,'r') as f:
                annot=json.load(f)
            for shape in annot['shapes']:
                if shape['label'] in self.cfg.PERSON_LABELS:

                    if shape["person"]==None:
                        label=shape['label']
                    else:
                        label=shape['person']
                        
                    if not label in label_dict.keys():
                        label_counter+=1
                        label_dict[label]=label_counter
                        
        print(f'found {label_counter} distinct labels')
        print(f"label_dict : {label_dict}")
        return label_dict
        
    def extract_boxes_and_labels(self,image,original_height,original_width,annot,do_masks):
        # get the height and width of the image
        image_width = image.shape[1]
        image_height = image.shape[0]
        
        # extract relevant objects from shapes in json
        boxes=[]
        labels=[]
        masks=[]
        for shape in annot['shapes']:
            if shape['label'] in self.cfg.PERSON_LABELS:     # this is just removed for the head check
            #if shape['label'] in SEARCH_LABELS:
                points=np.asarray(shape['points'])
                points=self.recalculate_shape_pixels(points,original_height,original_width)
                x,y,w,h=cv2.boundingRect(points.astype('int32'))
                xmin_final = max(round(x)-1,0)
                xmax_final = min(round(x+w)+1,self.width)                
                ymin_final = max(round(y)-1,0)
                ymax_final = min(round(y+h)+1,self.height)
                box=[xmin_final, ymin_final, xmax_final, ymax_final]
                boxes.append(box)
                if shape['person']: 
                    labels.append(shape['person'])
                else:
                    labels.append(shape['label']) 
                if do_masks:
                    masks.append(self.get_mask(points))
        #print(f"Number of boxes in target : {len(boxes)}")
        #print(labels)
        return boxes,labels,masks
    
class PersonDescriptionDataset(PersonObjectDataset):
    def __init__(self, split_dir, cfg, transforms=None, load_dataset=False, dataset_name="persondescription"):
        super().__init__(split_dir, cfg, transforms, load_dataset, dataset_name)        
        self.csv_path=cfg.CSV_DIR
        if load_dataset:
            print(f'loading {split_dir["mode"]} descriptions')
            with shelve.open(cfg.DATASET_DIR+dataset_name) as shelf:
                self.description_lookup=shelf['descriptions']
        else:
            specific=pd.read_csv(self.csv_path+'specific.csv',usecols=['Serial Number Painting','Decoration'])
            general=pd.read_csv(self.csv_path+'general.csv',usecols=['Serial Number Painting','Decoration'])
            merged=pd.concat([specific,general],ignore_index=True)
            self.description_lookup=dict(zip(merged['Serial Number Painting'].astype(str),merged['Decoration']))
            with shelve.open(cfg.DATASET_DIR+dataset_name) as shelf:
                shelf['descriptions']=self.description_lookup
    
    def __getitem__(self, idx):
        image_resized,target,image_original,reverse_label_dict=super().__getitem__(idx)
        image_name = self.all_images[idx]
        serial_number=image_name.split('-')[0]
        try:
            description=self.description_lookup[serial_number]
        except:
            description=""
        return image_resized, target, description, image_original, reverse_label_dict
                
    
    
class AllObjectsDataset(DefaultObjectDataset): 
    def __init__(self, split_dir, cfg, transforms=None, load_dataset=False, dataset_name="allobject"):
        super().__init__(split_dir, cfg, transforms=transforms, load_dataset=load_dataset, dataset_name=dataset_name) 
        
    def prepare_label_dict(self):
        print("allobjectsdataset prpare_label_dict method signal")
        label_dict={
            '__background__':0,
            }
        label_counter=0
        for annot_filename in self.all_jsons:
            annot_file_path = os.path.join(self.json_dir, annot_filename)
            with open(annot_file_path,'r') as f:
                annot=json.load(f)
            for shape in annot['shapes']:
                label=shape['label']
                if self.cfg.USE_ONLY_SPECIFIC_LABELS and not label in self.cfg.SPECIFIC_LABELS:
                    if not label in self.cfg.PERSON_LABELS:
                        continue 
                
                if label in self.cfg.PERSON_LABELS:
                    label='person'
                if not label in label_dict.keys():
                    label_counter+=1
                    label_dict[label]=label_counter
        print(f'found {label_counter} distinct labels')
        print(f"label_dict : {label_dict}")
        return label_dict
    
    '''   def check_if_shape_is_relevant(self,shape):
        label=shape['label']
        
        #check if label is in specific labels
        if self.cfg.USE_ONLY_SPECIFIC_LABELS and not label in self.cfg.SPECIFIC_LABELS and not label in self.cfg.PERSON_LABELS:
            return False,label
        else:     
            #check if label that should be child of a person is a child of a person
            if label in self.cfg.LABELS_CHILDREN_OF_PERSON:
                if shape['parent']:
                    parent=shape['parent']
                    for person_label in self.cfg.PERSON_LABELS:
                        if person_label in parent:
                            return True,label  
                    return False,label
                else:
                    return False,label
            #check if label is a person as defined in PERSON_LABELS  
            elif label in self.cfg.PERSON_LABELS:
                return True,'person'
            else:
                return True,label
            
        print("\n\nHOW DID I GET HERE?????\n\n")'''

    def check_if_shape_is_relevant(self,shape):
        label=shape['label']
        parent=shape['parent']
        
        if self.cfg.USE_ONLY_SPECIFIC_LABELS:
            if not label in self.cfg.SPECIFIC_LABELS and not label in self.cfg.PERSON_LABELS:
                return False,label
            elif not label in self.cfg.SPECIFIC_LABELS and label in self.cfg.PERSON_LABELS:      
                label='person'
                return True,label
            elif label in self.cfg.SPECIFIC_LABELS and not label in self.cfg.PERSON_LABELS: 
                if label in self.cfg.LABELS_CHILDREN_OF_PERSON:
                    if parent:
                        if type(parent)==list:
                            parent=parent[0]
                        for person_label in self.cfg.PERSON_LABELS:
                            if person_label in parent:
                                return True,label
                    return False,label
                return True,label
            
        raise Exception("got to the end of the function check_if_shape_is_relevant even though that should be impossible")
            
    def extract_boxes_and_labels(self,image,original_height,original_width,annot,do_masks): 
        # get the height and width of the image
        image_width = image.shape[1]
        image_height = image.shape[0]
        
        # extract relevant objects from shapes in json
        boxes=[]
        labels=[]  
        masks=[]
        for shape in annot['shapes']:
            label_relevant,label=self.check_if_shape_is_relevant(shape)
            if not label_relevant:
                continue
            points=np.asarray(shape['points'])
            points=self.recalculate_shape_pixels(points,original_height,original_width)
            x,y,w,h=cv2.boundingRect(points.astype('int32'))
            xmin_final = max(round(x)-1,0)
            xmax_final = min(round(x+w)+1,self.width)                
            ymin_final = max(round(y)-1,0)
            ymax_final = min(round(y+h)+1,self.height)
            box=[xmin_final, ymin_final, xmax_final, ymax_final]
            boxes.append(box)
            if label=='creaturee': label='creature'
            labels.append(label)
            if do_masks:
                masks.append(self.get_mask(points))
        return boxes,labels,masks
                
class AllObjectsDatasetDescriptions(AllObjectsDataset):
    def __init__(self, split_dir, cfg, transforms=None, load_dataset=False, dataset_name="allobjectdescriptions",):
        super().__init__( split_dir, cfg, transforms=transforms, load_dataset=load_dataset, dataset_name=dataset_name)
        self.csv_path=cfg.CSV_DIR
        if load_dataset:
            print(f'loading {split_dir["mode"]} descriptions')
            with shelve.open(cfg.DATASET_DIR+dataset_name) as shelf:
                self.description_lookup=shelf['descriptions']
        else:
            specific=pd.read_csv(self.csv_path+'specific.csv',usecols=['Serial Number Painting','Decoration'])
            general=pd.read_csv(self.csv_path+'general.csv',usecols=['Serial Number Painting','Decoration'])
            merged=pd.concat([specific,general],ignore_index=True)
            self.description_lookup=dict(zip(merged['Serial Number Painting'].astype(str),merged['Decoration']))
            with shelve.open(cfg.DATASET_DIR+dataset_name) as shelf:
                shelf['descriptions']=self.description_lookup
            
    
    def __getitem__(self, idx):
        image_resized,target,image_original,reverse_label_dict=super().__getitem__(idx)
        image_name = self.all_images[idx]
        serial_number=image_name.split('-')[0]
        try:
            description=self.description_lookup[serial_number]
        except:
            description=""
        return image_resized, target, description, image_original, reverse_label_dict
  
                           
class InvalidDatasetTypeException(Exception):
    pass
class InvalidDatasetModeException(Exception):
    pass
   
# prepare the final datasets and data loaders

def create_dataset(cfg, dataset_mode='train',load_dataset=False,dataset_name='person_object'):
    if dataset_mode=='train':
        split=cfg.TRAIN_SPLIT
        transform=get_train_transform()        
    elif dataset_mode=='val':
        split=cfg.VAL_SPLIT
        transform=get_valid_transform()
    elif dataset_mode=='test':
        split=cfg.TEST_SPLIT
        transform=get_test_transform()
    else:
        raise InvalidDatasetModeException
    
    if cfg.DATASET=='person':
        dataset = PersonObjectDataset(split, cfg,  transform, load_dataset=load_dataset, dataset_name=dataset_name)
    elif cfg.DATASET=='persondescriptions':
        dataset = PersonDescriptionDataset(split,cfg, transform, load_dataset=load_dataset, dataset_name=dataset_name)
    elif cfg.DATASET=='all':
        dataset = AllObjectsDataset(split, cfg,  transform, load_dataset=load_dataset, dataset_name=dataset_name)
    elif cfg.DATASET=='alldescriptions':
        dataset = AllObjectsDatasetDescriptions(split, cfg, transform, load_dataset=load_dataset, dataset_name=dataset_name)
    else:
        raise InvalidDatasetTypeException
    
    return dataset

def create_loader(dataset,cfg,shuffle=False):
    loader = DataLoader(
        dataset,
        batch_size=cfg.BATCH_SIZE,
        shuffle=shuffle,
        num_workers=cfg.NUM_WORKERS,
        collate_fn=collate_fn
    )
    return loader
    
print("datasets.py says hello")